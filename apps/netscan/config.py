"""
Scan network and return used IPs.
"""
import nmap
import alba.api.process


class Config(alba.api.process.Config):
    """
    Configuration for netscan application.
    """
    appname = 'netscan'


@alba.api.process.route('default')
def execute(args):
    """
    Find used IPs.
    """
    nm_sc = nmap.PortScanner()
    nm_sc.scan(hosts=args, arguments='-n -sP -PE -PA21,23,80,3389')
    hosts_list = [(x, nm_sc[x]['status']['state']) for x in nm_sc.all_hosts()]
    print repr(hosts_list)


