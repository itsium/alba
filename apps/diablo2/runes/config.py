"""
Diablo 2 Discover runes.
"""
import lxml.html
import urllib2
import alba.api.process


__url__ = 'http://diablo2.judgehype.com/index.php?page=objets-magiques-runes2'
__xpath_name__ = "//td/a[2][contains(@href,'images.php?img=')]"
__xpath_runes__ = "//tr/td[2]/font[@class='altcolor']"
__xpath_item__ = "//table[@class='contenu']/*/td[position() mod 3 = 0]"
__xpath_bonus__ = "//table[@class='contenu']/*/td[position() mod 4 = 0]"

class Config(alba.api.process.Config):
    """
    Configuration for diablo 2 runes discover.
    """
    appname = 'diablo2 runes'
    description = 'View runes'


def crawl_http(url):
    """
    Crawl given url and return content.
    """
    request = urllib2.Request(url)
    response = urllib2.urlopen(request)
    return response.read()


def xpath_find(name, rule, result, root):
    """
    Add xpath rules results to global results.
    """
    length = len(result)
    i = 0
    print len(root.xpath(rule))
    for match in root.xpath(rule):
        if i < length:
            result[i][name] = ''
            for txt in match.itertext():
                result[i][name] += txt
            result[i][name] = result[i][name].strip()
        i += 1
    return result


@alba.api.process.route('crawl_runes_words')
def crawl_runes_words():
    """
    Crawl runes words.
    """
    data = crawl_http(__url__)
    root = lxml.html.fromstring(data)
    result = []
    for i in root.xpath(__xpath_name__):
        result.append(dict())
    result = xpath_find('name', __xpath_name__, result, root)
    result = xpath_find('rune', __xpath_runes__, result, root)
    result = xpath_find('item', __xpath_item__, result, root)
    result = xpath_find('bonus', __xpath_bonus__, result, root)
    return result
