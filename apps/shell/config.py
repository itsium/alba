"""
Execute shell commands.
"""
import subprocess
import alba.api.process


class Config(alba.api.process.Config):
    """
    Configuration for shell application.
    """
    appname = 'shell'
    description = 'Execute given commands to server and return results.'


@alba.api.process.route('default')
def execute(api, args):
    """
    Execute given command and return output.
    """
    cmd = args.split()
    result = subprocess.check_output(cmd)
    return result
