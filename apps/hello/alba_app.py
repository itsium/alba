"""
Return hello world.
"""
import alba_wrapper
import alba.api.process


class App(alba.api.process.App):
    """Configuration for hello application."""
    name = 'hello_world'
    category = 'unittests'
    process_pool = 5
    synopsis = 'Perhaps you should say hello before introducing yourself.'
    socket = 'tcp://127.0.0.1:50003'
    version = '0.1'
    description = """
Perhaps you should say hello before introducing yourself.
"""


app = App(__name__)


@app.add_argument('name', nargs='+',
                  help='Single or multiple strings.')
@app.route('default')
def default(api, args):
    """Print given name and return output."""
    name = args.get('name', [])
    if type(name) is list:
        name = ' '.join(name)
    return 'Hello %s !' % str(name)


app.boot(alba_wrapper.get_config())
