"""Alba wrapper."""
import os
import sys
import argparse
import inspect


def get_socket():
    """Return socket address to core."""
    return args.get('socket')


def get_config():
    """Return parsed configuration."""
    return args


def parse_path(args):
    """Parse given args and return path.
    If path is incorrect, print error and exit.
    """
    path = args.get('path')
    if path[-1:] == '/':
        path = path[:-1]
    if path.startswith('/'):
        path = os.path.abspath(path)
    else:
        path = os.path.abspath('%s/%s' % (os.getcwd(), path))
    if os.path.isdir(path) is False:
        print 'Error: path "%s" is incorrect.' % path
        sys.exit(1)
    return os.path.dirname(path)


def parse_config():
    """Parse command line configuration."""
    frm = inspect.stack()
    if len(frm) < 3:
        return {}
    mod = inspect.getmodule(frm[2][0])
    if mod.__name__ != '__main__':
        return {}
    parser = argparse.ArgumentParser(prog='alba_wrapper')
    parser.add_argument('--socket', '-s', required=True,
                        help='%s%s%s' % ('The address string of the core socket.\n',
                                         'This has the form "protocol://interface:port".\n',
                                         'Example: "tcp://127.0.0.1:5555".'))
    parser.add_argument('--path', '-p', required=True,
                        help='%s%s' % ('The absolute path to the core folder.\n',
                                       'Example: "/home/user/alba".'))
    parser.add_argument('--debug', '-d', required=False,
                        default=False, type=bool,
                        help='Debug flag.')
    args = vars(parser.parse_args())
    path = parse_path(args)
    if path not in sys.path:
        sys.path.insert(0, path)
    app_path = os.path.dirname(__file__)
    return {
        'core_socket': args.get('socket'),
        'core_path': path,
        'app_path': app_path,
        'debug': args.get('debug')}


args = parse_config()
