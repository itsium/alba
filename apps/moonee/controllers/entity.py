"""
alba moonee entity controller.
"""
import json
import alba.services.web.helpers.args as args

from flask import request
from alba.apps.moonee.common import __db__
from alba.services.web.helpers.database import Collection


__c__ = None


def get_db():
    """Return collection."""
    global __c__

    if __c__ is None:
        __c__ = Collection(__db__, 'entities')
    return __c__


def search():
    """Return entities from database."""
    sorts = []
    filters = dict()
    if 'sort' in request.args:
        sorts = args.extjs_sorts_to_pymongo(json.loads(request.args['sort']))
    if 'filter' in request.args:
        filters = args.extjs_filters_to_pymongo(json.loads(request.args['filter']))
    records = get_db().search(sort=sorts, criteria=filters)
    results = {'success': True,
               'total': len(records),
               'data': records}
    return json.dumps(results)


def add():
    """Add given data to entity table."""
    data = json.loads(request.data)
    if data.get('id', '') != '':
        if len(data.keys()) == 1:
            return remove(data)
        return edit(data)
    record = get_db().add(data)
    results = {'success': True,
               'data': record}
    return json.dumps(results)


def edit(data):
    """Edit given data from its id."""
    record = get_db().edit(data.get('id', ''), data)
    results = {'success': True,
               'data': record}
    return json.dumps(results)


def remove(data):
    """Remove given data from database."""
    get_db().remove(data.get('id', ''))
    results = {'success': True}
    return json.dumps(results)
