"""
Alba index blueprint.
"""
import controllers.entry
import controllers.entity

from flask import Blueprint, render_template, abort, request
from jinja2 import TemplateNotFound


__moonee__ = Blueprint('moonee', __name__,
                       static_folder='static',
                       static_url_path='/static',
                       template_folder='templates',
                       url_prefix='/moonee')


@__moonee__.route('/')
def index():
    """Show index template."""
    try:
        return render_template('moonee/index.html')
    except TemplateNotFound:
        abort(404)


@__moonee__.route('/entry.json', methods=['GET', 'POST'])
def entry():
    """Store wrapper."""
    if request.method == 'GET':
        return controllers.entry.search()
    else:
        return controllers.entry.add()


@__moonee__.route('/entity.json', methods=['GET', 'POST'])
def entity():
    """Store wrapper."""
    if request.method == 'GET':
        return controllers.entity.search()
    else:
        return controllers.entity.add()


@__moonee__.route('/entity/update_balance.json')
def entity_update_balance():
    """Update balance from all entities."""
    return controllers.entity.update_balance()
