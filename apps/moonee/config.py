"""
alba moonee.
"""
import alba.api.process
import views

class Config(alba.api.process.Config):
    """
    Configuration for moonee.
    """
    appname = 'moonee'
    category = 'apps'
    description = 'Web interface for money management'


@alba.api.process.route('default')
def register_blueprint(rpc, args=None):
    """
    Register blueprint and enable commons blueprint too.
    """
    rpc.execute({'app': 'alba.services.web',
                 'action': 'add_module',
                 'args': {'import': 'alba.services.web.commons.views',
                          'name': '__commons__'}})
    rpc.execute({'app': 'alba.services.web',
                 'action': 'add_module',
                 'args': {'import': 'alba.apps.moonee.views',
                          'name': '__moonee__'}})
    return True
