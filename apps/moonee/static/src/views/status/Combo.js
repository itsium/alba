Ext.define('App.views.status.Combo', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
	'Ext.data.Store',
	'Ext.form.field.ComboBox'
    ],
    alias: 'widget.app.views.status.combo',

    initComponent: function() {
	this.store = Ext.create('Ext.data.Store', {
	    fields: ['id', 'text', 'icon'],
	    data: [{
		id: 'unpaid',
		text: t('Unpaid'),
		icon: 'icon-no-small'
	    }, {
		id: 'paid',
		text: t('Paid'),
		icon: 'icon-yes-small'
	    }]
	});
	this.valueField = 'id';
	this.displayField = 'text';
	this.query = 'local';
	this.forceSelection = true;
	this.listConfig = {
	    getInnerTpl: function() {
		return '<div class="div-icon {icon}" style="display: inline-block;">&nbsp;</div>{text}';
	    }
	};
	this.callParent(arguments);
    }
});