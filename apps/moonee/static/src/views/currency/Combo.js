Ext.define('App.views.currency.Combo', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
	'Ext.data.Store',
	'Ext.form.field.ComboBox'
    ],
    alias: 'widget.app.views.currency.combo',

    initComponent: function() {
	this.store = Ext.create('Ext.data.Store', {
	    fields: ['id', 'text', 'sign', 'icon'],
	    data: [{
		id: 'yuan',
		text: t('Yuan'),
		sign: '&yen;',
		icon: 'icon-currency-yuan'
	    }, {
		id: 'dollar',
		text: t('Dollar'),
		sign: '$',
		icon: 'icon-currency-dollar'
	    }, {
		id: 'euro',
		text: t('Euro'),
		sign: '&euro;',
		icon: 'icon-currency-euro'
	    }, {
		id: 'pound',
		text: t('Pound'),
		sign: '&pound;',
		icon: 'icon-currency-pound'
	    }],
	    sorters: [{
		property: 'id',
		direction: 'ASC'
	    }]
	});
	this.valueField = 'id';
	this.displayField = 'text';
	this.query = 'local';
	this.forceSelection = true;
	this.listConfig = {
	    getInnerTpl: function() {
		return '<div class="div-icon {icon}" style="display: inline-block;">&nbsp;</div>{text} {sign}';
	    }
	};
	this.callParent(arguments);
    }
});