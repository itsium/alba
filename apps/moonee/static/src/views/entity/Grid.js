Ext.define('App.views.entity.Grid', {
    extend: 'Common.controllers.GridEditing',
    requires: [
        'App.stores.Entity',
        'Common.controllers.GridEditing'
    ],
    alias: 'widget.app.views.entity.grid',

    initComponent: function() {
        this.title = t('Entities');
        this.iconCls = 'icon-entity';
        this.layout = 'fit';
        this.store = Ext.create('App.stores.Entity');
        this.columns = [{
            text: t('Id'),
            width: 100,
            sortable: true,
            dataIndex: 'id',
            hidden: true
        }, {
	    text: t('Icon'),
	    width: 30,
	    sortable: true,
	    dataIndex: 'icon',
	    renderer: function(value) {
		if (value != '') {
                    return '<div class="div-icon icon-' + value + '"></div>';
		}
		return '';
	    }
	}, {
            text: t('Name'),
            flex: 1,
            sortable: true,
            dataIndex: 'name',
            field: {
                xtype: 'textfield',
		emptyText: t('Enter a name') + '...',
		allowBlank: false
            }
	}];
	this.callParent(arguments);
    }
});