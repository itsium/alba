Ext.define('App.views.entry.Grid', {
    extend: 'Common.controllers.GridEditing',
    requires: [
        'App.stores.Entry',
	'App.stores.Entity',
	'App.views.status.Combo',
	'App.views.currency.Combo',
        'Common.controllers.GridEditing'
    ],
    alias: 'widget.app.views.entry.grid',

    initComponent: function() {
        this.title = t('Entries');
        this.iconCls = 'icon-entry';
        this.layout = 'fit';
        this.store = Ext.create('App.stores.Entry');
        this.columns = [{
            text: t('Id'),
            width: 100,
            sortable: true,
            dataIndex: 'id',
            hidden: true
        }, {
	    text: t('Status'),
	    width: 30,
	    sortable: true,
	    dataIndex: 'status',
	    align: 'center',
	    renderer: function(value, metaData, record, rowIndex, colIndex) {
		var editor = this.columns[colIndex].getEditor();
		var status = editor.getStore().findRecord('id', value);
                if (Ext.isObject(status)) {
		    return '<div class="div-icon ' + status.get('icon') + '"></div>';
                }
                return '<div class="div-icon icon-no-small"></div>';
	    },
	    field: {
		xtype: 'app.views.status.combo'
	    }
	}, {
            text: t('Label'),
            flex: 1,
            sortable: true,
            dataIndex: 'label',
            field: {
                xtype: 'textfield',
		emptyText: t('Enter a label') + '...',
		allowBlank: false
            }
        }, {
            text: t('Amount'),
            width: 300,
            sortable: true,
            dataIndex: 'amount',
	    align: 'right',
	    field: {
		xtype: 'numberfield',
		emptyText: t('Enter an amount') + '...',
		allowBlank: false
	    }
	}, {
	    text: t('Currency'),
	    width: 20,
	    sortable: true,
	    dataIndex: 'currency',
	    align: 'left',
	    renderer: function(value, metaData, record, rowIndex, colIndex) {
		var editor = this.columns[colIndex].getEditor();
		var currency = editor.getStore().findRecord('id', value);
		if (Ext.isObject(currency)) {
		    return currency.get('sign');
		}
		return '&euro;';
	    },
	    field: {
		xtype: 'app.views.currency.combo'
	    }
	}, {
	    text: t('To'),
	    flex: 1,
	    sortable: true,
	    dataIndex: 'entity_id',
	    renderer: function(value, metaData, record) {
		if (value.length > 1 &&
		    Ext.isObject(record.get('entity')) === true) {
		    return record.get('entity').name;
		}
		return value;
	    },
	    field: {
		xtype: 'combo',
		store: Ext.create('App.stores.Entity'),
		displayField: 'name',
		valueField: 'id',
		allowBlank: false,
		emptyText: t('Choose an entity') + '...'
	    }
	}, {
	    xtype: 'datecolumn',
	    format: 'd/m/Y',
	    text: t('Date'),
	    width: 150,
	    sortable: true,
	    dataIndex: 'date',
	    field: {
		xtype: 'datefield',
		format: 'd/m/Y H:i:s',
		allowBlank: false
	    }
	}];
	this.callParent(arguments);
    }
});