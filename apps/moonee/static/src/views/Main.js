Ext.define('App.views.Main', {
    extend: 'Ext.container.Viewport',
    requires: [
        'Ext.container.Viewport',
	'Ext.tab.Panel',
	'App.views.entry.Grid',
	'App.views.entity.Grid'
    ],
    alias: 'widget.app.views.main',

    initComponent: function() {
        this.layout = {
            type: 'border',
            padding: 2
        };
        this.items = [{
            xtype: 'toolbar',
            layout: 'hbox',
            region: 'north',
            border: '0 0 0 0',
            bodyStyle: {
                background: '#e0e0e0'
            },
            items: [{
                xtype: 'container',
                html: '<div class="logo">Moonee</div>'
            }, {
                xtype: 'container',
                flex: 1
            }]
        }, {
	    xtype: 'tabpanel',
	    region: 'center',
	    items: [{
		xtype: 'app.views.entry.grid'
	    }, {
		xtype: 'app.views.entity.grid'
	    }]
	}];
	this.callParent(arguments);
    }
});