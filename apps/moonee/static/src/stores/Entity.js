Ext.define('App.stores.Entity', {
    extend: 'Ext.data.Store',
    requires: [
        'App.models.Entity',
        'Ext.data.Store'
    ],

    constructor: function(config) {
        this.autoLoad = false;
        this.autoSync = false;
        this.pageSize = 50;
	this.remoteSort = true;
	this.remoteFilter = true;
        this.model = 'App.models.Entity';
	this.sorters = [{
	    property: 'name',
	    direction: 'ASC'
	}];
        this.proxy = {
            type: 'ajax',
            url: '/moonee/entity.json',
            format: 'json',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'total',
		successProperty: 'success'
            },
            writer: {
                type: 'json',
                writeAllFields: false
            }
        };
	this.callParent(arguments);
    }
});