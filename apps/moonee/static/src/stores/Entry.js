Ext.define('App.stores.Entry', {
    extend: 'Ext.data.Store',
    requires: [
        'App.models.Entry',
        'Ext.data.Store'
    ],

    constructor: function(config) {
        this.autoLoad = false;
        this.autoSync = false;
	this.remoteSort = true;
	this.remoteFilter = true;
        this.pageSize = 50;
        this.model = 'App.models.Entry';
	this.sorters = [{
	    property: 'date',
	    direction: 'DESC'
	}];
        this.proxy = {
            type: 'ajax',
            url: '/moonee/entry.json',
            format: 'json',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'total',
		successProperty: 'success'
            },
            writer: {
                type: 'json',
                writeAllFields: false
            }
        };
	this.callParent(arguments);
    }
});