Ext.define('App.models.Entity', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.Model'
    ],
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'string'
    }, {
        name: 'name',
        type: 'string'
    }, {
        name: 'icon',
        type: 'string'
    }]
});