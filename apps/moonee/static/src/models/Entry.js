Ext.define('App.models.Entry', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.Model'
    ],
    idProperty: 'id',
    fields: [{
        name: 'id',
        type: 'string'
    }, {
        name: 'label',
        type: 'string'
    }, {
        name: 'amount',
        type: 'float',
	defaultValue: 0.0
    }, {
	name: 'currency',
	type: 'string',
	defaultValue: 'euro'
    }, {
	name: 'entity_id',
	type: 'string'
    }, {
	name: 'entity'
    }, {
        name: 'status',
        type: 'string',
	defaultValue: 'unpaid'
    }, {
	name: 'date',
	type: 'date',
	defaultValue: new Date()
    }],
    validations: [{
        type: 'inclusion',
        field: 'status',
        list: [
            'unpaid',
            'paid'
        ]
    }]
});