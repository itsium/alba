Ext.Loader.setConfig({
    enabled: true,
    paths: {
	'App': '/moonee/static/src',
	'Common': '/commons/static/src'
    }
});
Ext.require([
    'Common.controllers.I18N',
    'App.views.Main'
]);

Ext.onReady(function() {
    Common.controllers.I18N.path = '/moonee/static/src/lang/';
    Common.controllers.I18N.extPath = '/commons/static/extjs/';
    Common.controllers.I18N.on('langloaded', function() {
        var app = Ext.create('App.views.Main');
    });
    Common.controllers.I18N.setLang('en');
});
