"""
Manually crawlee launch.
"""
import api


def main():
    """
    Main.
    """
    pfields = [{'name': 'id',
                'xpath': "//div[@id='selection']/*/table/tbody/tr[@class='tr1' or @class='tr2']/td[1]/p/a/@href",
                'is_unique': True},
               {'name': 'title',
                'xpath': "//div[@id='selection']/*/table/tbody/tr[@class='tr1' or @class='tr2']/td[1]/p/a",
                'is_unique': False},
               {'name': 'note',
                'xpath': "//div[@id='selection']/*/table/tbody/tr[@class='tr1' or @class='tr2']/td[6]",
                'is_unique': False},
               {'name': 'date',
                'xpath': "//div[@id='selection']/*/table/tbody/tr[@class='tr1' or @class='tr2']/td[4]",
                'is_unique': False},
               {'name': 'type',
                'xpath': "//div[@id='selection']/*/table/tbody/tr[@class='tr1' or @class='tr2']/td[2]",
                'is_unique': False}]
    profile = {'url': 'http://www.jeuxvideo.com/articles/listes/tests-pc-noms.htm',#'http://www.jeuxvideo.com/articles/listes/tests-psp-noms.htm',
               'charset': 'iso-8859-1',
               'use_cache': True,
               'fields': pfields}
    data = api.http_profile(profile)
    records = []
    for field in profile.get('fields'):
        records = api.field_find(field, data, records)
    print repr(records)
    print len(records)


if __name__ == '__main__':
    main()
