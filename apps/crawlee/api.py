"""
HTTP Crawler api.
"""
import lxml.html
import urllib2
import cache


__user_agent__ = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1'


def http_get(url, use_cache=False, charset=None):
    """
    Crawl given url and return content.
    """
    if use_cache is True:
        data = cache.get(url)
        if data is not None:
            return data
    else:
        cache.delete(url)
    headers = {'User-Agent': __user_agent__}
    request = urllib2.Request(url, '', headers)
    response = urllib2.urlopen(request)
    data = response.read()
    if charset is None:
        #udata = data.decode('raw_unicode_escape')
        udata = data.decode('utf-8')
    else:
        udata = data.decode(charset)
    cache.add(url, udata)
    return udata


def http_profile(profile):
    """
    Crawl from given profile.
    """
    return http_get(profile.get('url'),
                    profile.get('use_cache', False),
                    profile.get('charset'))


def field_find(field, data, records=[]):
    """
    Find all field in given data and return records.
    """
    results = xpath_apply(field.get('xpath'), data)
    lresults = len(results)
    if len(records) == 0:
        for i in range(0, lresults):
            records.append({})
    lrecords = len(records)
    if lrecords != lresults:
        print '[!] Error for key: %s' % field.get('name')
        print '\tRecords found: %d\n\tResults found: %d' % (lrecords, lresults)
        return records
    for i in range(0, lrecords):
        records[i].update({field.get('name'): results[i]})
    return records


def xpath_apply(rule, data):
    """
    Apply given rule to data and return results.
    """
    root = lxml.html.fromstring(data)
    results = []
    for match in root.xpath(rule):
        if hasattr(match, 'text'):
            results.append(match.text)
        else:
            results.append(match)
    return results
