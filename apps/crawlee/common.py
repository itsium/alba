"""
Crawlee commons.
"""
import pymongo

__connection__ = pymongo.Connection()
__db__ = __connection__.crawlee
