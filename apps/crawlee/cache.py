"""
Crawler cache functions.
"""
from common import __db__ as db


def get(key):
    """
    Verify if given key was cached.
    """
    record = db.cache.find_one({"key": key.strip()})
    if record is None:
        return
    return record.get('data')


def add(key, data):
    """
    Add given data to cache.
    """
    result = db.cache.update({"key": key.strip()},
                             {"$set": {"data": data.encode('utf-8')}},
                             True)
    return result


def delete(key):
    """
    Delete given key from cache.
    """
    return db.cache.remove({"key": key.strip()})
