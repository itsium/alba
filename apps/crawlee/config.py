"""
HTTP Crawler.
"""
import alba.api.process


class Config(alba.api.process.Config):
    """
    Configuration for crawlee application.
    """
    appname = 'crawlee'
    description = 'HTTP Crawler profile.'


@alba.api.process.route('default')
def default(args):
    """
    Execute given command and return output.
    """
    return 'Hello %s !' % str(args)
