Ext.Loader.setConfig({
    enabled: true,
    paths: {
	'App': '/terminal/static/src'
    }
});
Ext.require([
    'App.views.header.Panel',
    'App.views.terminal.Panel',
    'Ext.container.Viewport'
]);

Ext.onReady(function() {
    Ext.QuickTips.init();

    Ext.create('Ext.container.Viewport', {
	layout: 'border',
	items: [{
	    xtype: 'app.views.header.panel',
	    region: 'north',
	    margins: '0 0 0 0'
	}, {
	    xtype: 'app.views.terminal.panel',
	    region: 'center',
	    margins: '0 0 0 0'
	}]
    });
});
