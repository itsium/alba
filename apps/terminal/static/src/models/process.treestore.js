Ext.define('App.models.process.TreeStore', {
    alias: 'widget.app.models.process.treestore',
    extend: 'Ext.data.TreeStore',

    constructor: function() {
	// privates
	this.isLoading = false;
	this.wasLoaded = false;
	// configuration
	this.autoLoad = false;
	this.proxy = {
	    type: 'ajax',
	    url: '/launcher/process/available'
	};
	this.root = {
	    text: 'Processes',
	    id: 'root',
	    expanded: false
	};
	this.folderSort = true;
        this.sorters = [{
            property: 'text',
            direction: 'ASC'
        }];
	this.callParent(arguments);
	this.on('beforeload', this._onBeforeLoad, this);
	this.on('load', this._onLoad, this);
    },

    _onBeforeLoad: function() {
	this.isLoading = true;
    },

    _onLoad: function() {
	this.isLoading = false;
	this.wasLoaded = true;
    }
});