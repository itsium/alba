Ext.define('App.models.Application', {
    extend: 'Ext.data.Model',
    fields: [{
	name: 'id'
    }, {
	name: 'name'
    }, {
	name: 'status',
	type: 'int'
    }, {
	name: 'icon'
    }, {
	name: 'description'
    }]
});

Ext.define('App.models.ApplicationStore', {
    extend: 'Ext.data.Store',
    widget: 'widget.app.models.applicationstore',

    constructor: function(config) {
	this.proxy = {
	    type: 'ajax',
	    url: '/launcher/app/list',
	    reader: {
		type: 'json',
		root: 'results'
	    }
	};
	this.autoLoad = true;
	this.model = 'App.models.Application';
	this.sorters = [{
	    property: 'name',
	    direction: 'ASC'
	}];
	this.callParent(arguments);
    }
});
