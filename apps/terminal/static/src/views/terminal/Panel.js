Ext.define('App.models.terminal.Command', {
    extend: 'Ext.data.Model',
    requires: [
	'Ext.data.Model'
    ],
    idProperty: 'id',
    fields: [{
	name: 'id',
	type: 'int'
    }, {
	name: 'loading',
	type: 'boolean',
	defaultValue: true
    }, {
	name: 'command',
	type: 'string'
    }, {
	name: 'success',
	type: 'boolean'
    }, {
	name: 'result',
	type: 'string'
    }]
});

Ext.define('App.stores.terminal.Commands', {
    extend: 'Ext.data.Store',
    requires: [
	'Ext.data.Store',
	'App.models.terminal.Command'
    ],
    constructor: function(config) {
	this.model = 'App.models.terminal.Command';
	this.autoLoad = false;
	this.autoSync = false;
	this.proxy = {
	    type: 'memory',
	    reader: {
		type: 'json',
		root: 'data'
	    }
	};
	this.callParent(arguments);
    }
});

Ext.define('App.views.terminal.Panel', {
    extend: 'Ext.panel.Panel',
    requires: [
	'Ext.panel.Panel',
	'Ext.view.View',
	'Ext.form.field.Text',
	'App.stores.terminal.Commands'
    ],
    alias: 'widget.app.views.terminal.panel',

    initComponent: function() {
	this.title = 'Terminal';
	this.layout = 'fit';
	this.store = Ext.create('App.stores.terminal.Commands');
	this.recordId = 1;
	this.dockedItems = [{
	    xtype: 'toolbar',
	    dock: 'top',
	    items: [{
		xtype: 'textfield',
		enableKeyEvents: true,
		emptyText: 'Command ...',
		listeners: {
		    scope: this,
		    keyup: function(field, e, eOpts) {
			if (e.getCharCode() === Ext.EventObject.ENTER &&
			    field.getValue().length > 0) {
			    this.sendCommand(field.getValue());
			    field.reset();
			}
		    }
		},
		flex: 1
	    }]
	}];
	this.items = [{
	    xtype: 'dataview',
	    layout: 'fit',
	    autoScroll: true,
	    tpl: new Ext.XTemplate(
		'<tpl for=".">',
		' <div class="command-item" style="margin: 1px;">',
		'  <pre>$&gt; {command}',
		'  <tpl if="loading == true">',
		'   </pre><img src="/externals/static/extjs/resources/themes/images/default/grid/loading.gif" alt="Loading..." /><br />',
		'  </tpl>',
		'  <tpl if="loading == false">',
		'   \n{result}</pre>',
		'  </tpl>',
		' </div>',
		'</tpl>'
	    ),
	    store: this.store,
	    itemSelector: 'div.command-item',
	    singleSelect: true
	}];
	this.callParent(arguments);
    },

    sendCommand: function(command) {
	var record = new (this.store.getProxy().getModel())();
	record.set('id', ++this.recordId);
	record.set('loading', true);
	record.set('command', command);
	this.store.insert(0, record);
	record.commit();
	Ext.Ajax.request({
	    url: '/terminal/exec?' + Ext.Object.toQueryString({
		q: command,
		id: record.get('id')
	    }),
	    scope: this,
	    success: function(response) {
		var result = Ext.decode(response.responseText);
		record.set('loading', false);
		record.set('success', result.success);
		record.set('result', result.data);
		record.commit();
	    }
	});
    }
});