Ext.define('App.views.process.Available', {
    alias: 'widget.app.views.process.available',
    extend: 'Ext.tree.Panel',

    initComponent: function() {
	this.contextMenu = Ext.create('Ext.menu.Menu', {
	    items: [{
		text: 'Run',
		iconCls: 'application-plus',
		listeners: {
		    scope: this,
		    click: this._onRunClicked
		}
	    }, {
		text: 'Stop',
		iconCls: 'application-minus',
		disabled: true,
		listeners: {
		    scope: this,
		    click: this._onStopClicked
		}
	    }, {
		text: 'Pause',
		iconCls: 'application-exclamation',
		disabled: true,
		listeners: {
		    scope: this,
		    click: this._onPauseClicked
		}
	    }],
	    setRecord: function(record) {
		this.record = record;
	    },
	    getRecord: function() {
		return this.record;
	    }
	});
	this.store = Ext.create('App.models.process.TreeStore');
	this.rootVisible = false;
	this.on('activate', this._onActivate, this);
	this.on('itemcontextmenu', this._onItemContextMenu, this);
	this.callParent(arguments);
    },

    _onActivate: function(cmp, options) {
	if (this.store.isLoading === false &&
	    this.store.wasLoaded === false) {
	    this.store.load();
	}
    },

    _onItemContextMenu: function(view, record, item, index, event, opts) {
	event.stopEvent();
	this.contextMenu.setRecord(record);
	this.contextMenu.showAt(event.getXY());
	return false;
    },

    _onRunClicked: function(item, event, options) {
    },

    _onStopClicked: function(item, event, options) {
    },

    _onPauseClicked: function(item, event, options) {
    }
});