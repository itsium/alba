Ext.define('App.views.appfinder.results.List', {
    extend: 'Ext.view.View',
    alias: 'widget.app.views.appfinder.results.list',

    initComponent: function() {
	this.tpl = Ext.create('Ext.XTemplate', '<div class="appfinder-list-container">',
			      '<tpl for=".">',
			      '<div class="appfinder-list-item {[xindex % 2 === 0 ? "even" : "odd"]}">',
			      ' <div class="appfinder-list-item-icon" style="',
			      '  background-image: url(/api/app/icon/{id});"></div>',
			      ' <div class="appfinder-list-item-name">',
			      '  {name}',
			      ' </div>',
			      ' <tpl if="status == 0">',
			      ' <span class="appfinder-list-item-status btn small">off</span>',
			      ' </tpl>',
			      ' <tpl if="status == 1">',
			      ' <span class="appfinder-list-item-status btn small info">idle</span>',
			      ' </tpl>',
			      ' <tpl if="status == 2">',
			      ' <span class="appfinder-list-item-status btn small primary">on</span>',
			      ' </tpl>',
			      ' <div style="clear: both"></div>',
			      '</div>',
			      '</tpl>',
			      '</div>');
	this.singleSelect = true;
	this.autoScroll = true;
	this.trackOver = true;
	this.overItemCls = 'appfinder-list-item-over';
	this.itemSelector = 'div.appfinder-list-item';
	this.emptyText = 'No application available';
	this.callParent(arguments);
    }
});