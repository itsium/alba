Ext.define('App.views.appfinder.results.Detail', {
    extend: 'Ext.view.View',
    alias: 'widget.app.views.appfinder.results.detail',

    initComponent: function() {
	this.tpl = Ext.create('Ext.XTemplate', '<tpl for=".">',
			      '<div class="appfinder-detail-item">',
			      ' <div class="appfinder-detail-item-icon" style="',
			      '  background-image: url(/api/app/icon/{id});"></div>',
			      ' <div class="appfinder-detail-item-name">',
			      '  {name}',
			      ' </div>',
			      ' <tpl if="status == 0">',
			      ' <span class="appfinder-detail-item-status btn small">off</span>',
			      ' </tpl>',
			      ' <tpl if="status == 1">',
			      ' <span class="appfinder-detail-item-status btn small info">idle</span>',
			      ' </tpl>',
			      ' <tpl if="status == 2">',
			      ' <span class="appfinder-detail-item-status btn small primary">on</span>',
			      ' </tpl>',
			      ' <div class="appfinder-detail-item-separator"></div>',
			      ' <div class="appfinder-detail-item-description">',
			      '  {description}',
			      ' </div><div style="clear: both"></div>',
			      '</div>',
			      '</tpl>');
	this.singleSelect = true;
	this.autoScroll = true;
	this.trackOver = true;
	this.overItemCls = 'appfinder-detail-item-over';
	this.itemSelector = 'div.appfinder-detail-item';
	this.emptyText = 'No application available';
	this.callParent(arguments);
    }
});