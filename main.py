"""
Main alba simple example.
"""
import alba


def main():
    """Start alba core and shell."""
    alba.start()


if __name__ == '__main__':
    main()
