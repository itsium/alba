"""
Application record.
"""
import os
import hashlib
import zmq
import alba
import alba.api.process
import alba.app.config
import alba.helpers.log as log
import alba.helpers.module as module
import alba.helpers.process
import alba.rpc.api.serializers as serializers


STATUS_OFF = 0
STATUS_STARTING = 1
STATUS_IDLE = 2
STATUS_ON = 3


class AppRecord(object):
    """Application container."""

    # Default application file for alba
    app_file = 'alba_app.py'

    def __init__(self, context):
        """Create new application."""
        self.errors = []
        self.processes = []
        self.status = STATUS_OFF
        self.module = None
        self.config = {}
        self.context = context
        self.commands = []
        self.working_cmd = {}
        self.config = alba.app.config.Config()
        self.config.set('id', self.generate_id())

    def __str__(self):
        """String call override."""
        return 'Application "%s" at "%s"' % (
            self.config.get('name'),
            self.config.get('path'))

    def __repr__(self):
        """Representative call override."""
        return str(self)

    def to_dict(self):
        """Convert this object to dictionnary."""
        d = self.config.to_dict()
        d['status'] = self.status
        return d

    def get_name(self):
        """Return application name."""
        return self.config.get('name')

    def get_fullname(self):
        """Return application full name."""
        return self.config.get('fullname')

    def local_load(self, path):
        """Load application configuration."""
        self.config.set('path', path)
        self.module = self._load_module()
        if self.module is False:
            return False
        self._load_config()
        self.config.set('path', path)
        self.config.set('icon', self.find_icon())
        return True

    def _load_module(self):
        """Try to load module."""
        log.debug('[AppRecord] "%s" - "%s"' % (self.config.get('name'),
                                               self.config.get('path')))
        result = module.load(self.app_file.replace('.py', ''),
                             self.config.get('path'))
        log.debug('[AppRecord] %s / %s' %
                  (repr(result), dir(result)))
        if result is None:
            return False
        return result

    def _load_config(self):
        """Load application module configuration."""
        app_config = None
        for varname in dir(self.module):
            var = getattr(self.module, varname)
            if isinstance(var, alba.api.process.App) is True:
                app_config = var
        log.debug('[CONFIG-APP] %s' % repr(app_config))
        return self.config.parse_from_module(app_config)

    def _init_sender(self):
        """Initialize sender."""
        self.serializer = serializers.JSON()
        self.sender = self.context.socket(zmq.PUSH)
        self.sender.setsockopt(zmq.LINGER, 0)
        self.sender.bind(self.config.get('socket'))
        return True

    def remote_load(self, app_config):
        """Remote loading of application configuration."""
        self._init_sender()
        self.status = STATUS_IDLE
        log.debug('[REMOTE_LOAD] %s' % repr(app_config))
        self.config.parse_from_dict(app_config)
        self.config.set('icon', self.find_icon())
        self._send_waiting_commands()
        return True

    def _send_waiting_commands(self):
        """Send waiting commands to application."""
        for command in self.commands:
            self.execute(command)
        self.commands = []

    def find_icon(self):
        """Find application icon."""
        icon_path = '%s/icon.png' % self.config.get('path')
        if os.path.isfile(icon_path):
            return icon_path
        return

    def generate_id(self):
        """Generate application id."""
        key = hashlib.sha1()
        key.update(repr(self))
        return key.hexdigest()

    def has_icon(self):
        """Check if application has icon.
        Return result.
        """
        if type(self.config.get('icon')) == str and \
                os.path.isfile(self.config.get('icon')):
            return True
        return False

    def get_id(self):
        """Return application id."""
        return self.config.get('id')

    def get_icon(self):
        """Return icon path."""
        return self.config.get('icon')

    def add_error(self, msg):
        """Add given error to list."""
        self.errors.append(msg)

    def start(self):
        """Start application."""
        self.status = STATUS_STARTING
        log.debug('[START] %s/%s -s %s -p %s -d %s' %
                  (self.config.get('path'), self.app_file,
                   alba.config.get('core_socket'),
                   alba.config.get('basedir'),
                   str(int(alba.config.get('core_debug')))))
        try:
            alba.helpers.process.pycall('%s/%s' % (self.config.get('path'),
                                                   self.app_file),
                                        ['-s', alba.config.get('core_socket'),
                                         '-p', alba.config.get('basedir'),
                                         '-d', str(int(alba.config.get('core_debug')))])
        except Exception as e:
            log.debug('[START] Exception: %s' % repr(e))
            return False
        return True

    def execute(self, command):
        """Execute given command."""
        if self.status == STATUS_OFF:
            self.start()
        if self.status == STATUS_STARTING:
            self.commands.append(command)
        else:
            self.status = STATUS_ON
            self.working_cmd[str(command.get('id'))] = True
            self.send(command)

    def finish(self, command_id):
        """Mark given command id as finish."""
        if self.working_cmd.get(command_id, None) is not None:
            del self.working_cmd[command_id]
        if len(self.working_cmd) < 1 and self.status == STATUS_ON:
            self.status = STATUS_IDLE

    def send(self, message):
        """Send action to process."""
        log.debug('[%s][SEND] %s' %
                  (self.get_fullname(), repr(message)))
        self.sender.send(self.serializer.encode(message))

    def quit(self):
        """Quit application."""
        if self.status == STATUS_OFF:
            if hasattr(self, 'sender'):
                self.sender.close()
            return True
        # @TODO ne pas envoyer plus de messages qu'il n'y a de process
        # Pourquoi j'ai fais ca: car parfois un ou deux process_pool
        # restent ouvert a cause d'une race condition
        for i in range(0, self.config.get('process_pool') * 2):
            self.send({'route': 'quit'})
        self.sender.close()
        self.status = STATUS_OFF
        return True
