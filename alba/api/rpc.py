"""
alba Application RPC API.
"""
import alba.api.core

from alba.rpc.helpers import rpc_method


class Api(object):

    def __init__(self, context):
        """
        Api app RPC server.
        """
        self.core = alba.api.core.Core(context)

    def __del__(self):
        """
        Cleaning.
        """
        pass

    @rpc_method
    def execute(self, commands):
        """Execute given commands."""
        return self.core.execute(commands)

    @rpc_method
    def remote_load(self, app_config):
        """Remote load application."""
        return self.core.remote_load(app_config)

    @rpc_method
    def get(self, appid):
        """
        Return application object from given id.
        """
        return self.core.get(appid)

    @rpc_method
    def get_from_name(self, appname):
        """
        Return application object from  its name.
        """
        return self.core.get_from_name(appname)

    @rpc_method
    def listall(self):
        """
        List all available applications.
        """
        return self.core.listall()

    @rpc_method
    def get_all(self):
        """
        Return all available applications.
        """
        return self.core.get_all()

    @rpc_method
    def find(self, folder='apps'):
        """
        Find and return all available applications
        from given folder.
        """
        return self.core.find(folder)

    @rpc_method
    def quit(self):
        """
        Quit all processes.
        """
        return self.core.quit()

    @rpc_method
    def get_routes(self, appname):
        """
        Get routes from given appname.
        """
        return self.core.get_routes(appname)

    @rpc_method
    def config_get(self, varname, default_value=None):
        """
        Get alba config variable.
        """
        return self.core.config_get(varname, default_value)

    @rpc_method
    def config_set(self, varname, value):
        """
        Set alba config variable.
        """
        return self.core.config_set(varname, value)

    @rpc_method
    def set_command_result(self, command_id, result):
        """
        Set command result.
        """
        return self.core.set_command_result(command_id, result)

    @rpc_method
    def log(self, command_id, message):
        """Send given message log to given command_id topic subscribers."""
        return self.core.log(command_id, message)
