"""
Alba core API.
"""
import os
import json
import alba
import zmq
import alba.helpers.module
import alba.helpers.log as log
import alba.rpc.api.serializers as serializers

from alba.api.apprecord import AppRecord


class Core(object):

    def __init__(self, context):
        """Core object used by RPC server."""
        self.apps = dict()
        self.context = context
        self.cmd_queue = {}
        self.serializer = serializers.JSON()
        self.publisher = self.context.socket(zmq.PUB)
        self.publisher.bind(alba.config.get('core_publisher'))
        self.command_ids = 0
        self._find_apps()

    def _find_apps(self):
        """Find apps from given configuration."""
        folders = self.config_get('core_apps_folder', None)
        if folders is not None:
            folders = folders.strip('\n\t ').split('\n')
            for folder in folders:
                folder = folder.strip('\n\t ')
                self.find(folder)
        return True

    def _add_to_command_queue(self, cid):
        """Add given command to command queue."""
        if self.cmd_queue.get(cid) is None:
            self.cmd_queue[cid] = []
        self.cmd_queue[cid].append(cid)
        return True

    def _pop_from_command_queue(self, cid):
        """Pop given cid from command queue."""
        if self.is_command_queue_empty(cid) is True:
            return False
        self.cmd_queue[cid].pop(0)
        if self.is_command_queue_empty(cid) is True:
            del self.cmd_queue[cid]
        return True

    def is_command_queue_empty(self, cid):
        """Return True if command queue is empty
        for given command id else False."""
        if self.cmd_queue.get(cid) is None:
            return True
        if len(self.cmd_queue[cid]) < 1:
            return True
        return False

    def execute(self, commands):
        """Execute given commands."""
        if type(commands) is not list:
            commands = [commands]
        for cmd in commands:
            if cmd.get('id') is None:
                cmd['id'] = str(self.command_ids)
                self.command_ids += 1
            self._add_to_command_queue(cmd.get('id'))
            self.app_execute(cmd)
        return commands

    def app_execute(self, command):
        """Find app from given command and execute it."""
        log.debug('[EXECUTE] %s' % repr(command))
        for appid, app in self.apps.iteritems():
            if app.get_fullname() == command.get('app'):
                app.execute(command)
                return True
        return False

    def get(self, appid):
        """Return application object from given id."""
        if self.apps.get(appid) is not None:
            return self.apps.get(appid)
        return None

    def get_from_name(self, appname):
        """Return application object from  its name."""
        for appid, app in self.apps.iteritems():
            if app.get_fullname() == appname:
                return app
        return

    def listall(self):
        """List all available applications."""
        apps = []
        for app in self.apps.itervalues():
            apps.append(app.to_dict())
        return apps

    def get_all(self):
        """Return all available applications."""
        apps = dict()
        for name, app in self.apps.iteritems():
            apps[name] = app.to_dict()
        return apps

    def find(self, path):
        """Find and return all available applications
        from given path.
        """
        if os.path.isdir(path) is False:
            return False
        alba.helpers.module.include_path(path)
        modules = []
        for dirname, dirnames, filenames in os.walk(path):
            if AppRecord.app_file in filenames:
                modules.append(dirname)
        for module_path in modules:
            app = AppRecord(self.context)
            if app.local_load(module_path) is True and \
                    self.apps.get(app.get_id()) is None:
                self.apps[app.get_id()] = app
        return self.apps

    def quit(self):
        """Quit all processes."""
        for appid, app in self.apps.iteritems():
            app.quit()
        self.publisher.close()
        socket = self.context.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, 0)
        socket.connect(self.config_get('core_socket'))
        socket.send('quit')
        socket.close()
        return True

    def remote_load(self, app_config):
        """Remote load application."""
        app = self.get_from_name(app_config.get('fullname', ''))
        if app is None:
            app = AppRecord(self.context)
        app.remote_load(app_config)
        self.apps[app.get_id()] = app
        return True

    def get_routes(self, appname):
        """Get routes from given appname."""
        app = self.get_from_name(appname)
        routes = dict()
        if app is not None:
            routes = app.config.get('routes')
            log.debug('[API] get_routes (before): %s' % repr(routes))
            results = {}
            for name, doc in routes.iteritems():
                results[name] = doc
            log.debug('[API] get_routes (after): %s' % repr(results))
            return results
        return dict()

    def config_get(self, varname, default_value=None):
        """Get alba config variable."""
        return alba.config.get(varname, default_value)

    def config_set(self, varname, value):
        """Set alba config variable."""
        alba.config.set(varname, value)
        return True

    def set_command_result(self, command_id, result):
        """Set command result."""
        log.debug('[SET_COMMAND_RESULT] %s "%s"' % (repr(command_id), repr(result)))
        self._pop_from_command_queue(command_id)
        if result is not None:
            self.publisher.send_multipart([command_id, self.serializer.encode(result)])
        if self.is_command_queue_empty(command_id) is True:
            # @TODO opti a faire ici. Ne chercher que les apps concerner par ce command_id.
            for app in self.apps.itervalues():
                app.finish(command_id)
            self.publisher.send_multipart([command_id, 'finish'])
        return True

    def log(self, command_id, message):
        """Send given message to given command_id topic."""
        self.publisher.send_multipart([command_id, self.serializer.encode(message)])
        return True
