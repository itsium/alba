"""
Python Process API.
"""
import os
import alba
import alba.app.config
import alba.helpers.args
import alba.rpc.client
import alba.helpers.log as log
import alba.helpers.process as process
import alba.rpc.api.serializers as serializers
import zmq

from multiprocessing import Process


class App(object):
    """Alba process configuration class.
    Calls from alba as target for new Process.
    """
    def __init__(self, import_name):
        if hasattr(self, 'category'):
            self.prefix = '[alba][%s][%s]' % (self.category,
                                              self.name)
        else:
            self.prefix = '[alba][%s]' % self.name
        self.import_name = import_name
        self.store = dict()
        self.config = alba.app.config.Config()
        self._init_config()

    def _init_config(self):
        """Initialize configuration dictionnary."""
        if hasattr(self, 'process_pool') is False:
            self.process_pool = 1
        if hasattr(self, 'routes') is False:
            self.routes = dict()
        if hasattr(self, 'events') is False:
            self.events = dict()
        return True

    def set(self, variable_name, value):
        """Set value to store."""
        self.store[variable_name] = value

    def get(self, variable_name, default_value=None):
        """Return value from store.
        If not exists, default_value is returned.
        """
        return self.store.get(variable_name, default_value)

    def start(self, core_socket=None):
        """Start processing queue."""
        process.set_title(self.prefix)
        self.processes = []
        if self.process_pool < 1:
            self.process_pool = 1
        for i in range(0, self.process_pool):
            num = i + 1
            self.processes.append(Process(target=self._start_pool,
                                          args=(num, core_socket, self.socket)))
        for i in range(0, self.process_pool):
            self.processes[i].start()
        self.send_config(core_socket)
        for i in range(0, self.process_pool):
            if self.processes[i].is_alive():
                self.processes[i].join()

    def _start_pool(self, num, core_socket, listener_config):
        """Start pool."""
        logname = self.prefix.replace('[', '').replace(']', '_')
        log.create('%s%d' % (logname, num))
        context = zmq.Context.instance()
        listener = context.socket(zmq.PULL)
        listener.setsockopt(zmq.LINGER, 0)
        listener.connect(listener_config)
        api = alba.rpc.client.Client()
        api.connect(core_socket, context)
        serializer = serializers.JSON()
        is_running = True
        while is_running is True:
            process.set_title('%s %d | waiting' % (self.prefix, num))
            cmd = listener.recv()
            cmd = serializer.decode(cmd)
            log.debug('%s-%d: %s\n' % (self.prefix, num, repr(cmd)))
            process.set_title('%s %d | working' % (self.prefix, num))
            api.use_command_id(cmd.get('id'))
            if self.handle_command(cmd, api) is False:
                is_running = False
            api.unuse_command_id(cmd.get('id'))
        listener.close()
        api.close()
        context.term()

    def handle_command(self, cmd, api):
        """Handle given command and return result."""
        if cmd.get('route') == 'quit':
            return False
        else:
            function = self.routes.get(cmd.get('route'))
            if function is None:
                api.set_command_result(cmd.get('id'),
                                       'No route "%s".' % cmd.get('route'))
            args = alba.helpers.args.check_args(cmd, function)
            if callable(function) and type(args) is dict:
                try:
                    result = function(api, args)
                    api.set_command_result(cmd.get('id'), result)
                except:
                    alba.helpers.log.exception()
                    api.set_command_result(cmd.get('id'), 'Failed')
            else:
                api.set_command_result(cmd.get('id'), args)
        return True

    def boot(self, config):
        """Boot application."""
        if self.import_name != '__main__':
            return False
        logname = self.prefix.replace('[', '').replace(']', '_')
        log.create(logname)
        self.set('core_socket', config.get('core_socket'))
        self.config.parse_from_module(self)
        self.start(self.get('core_socket'))
        return True

    def send_config(self, core_socket):
        """Send configuration to core."""
        api = alba.rpc.client.Client()
        api.connect(core_socket)
        log.debug('[SEND_CONFIG] %s' % repr(self.config))
        result = api.remote_load(self.config.to_dict())
        log.debug('[SEND_CONFIG] result: %s' % repr(result))
        api.close()
        log.debug('[SEND_CONFIG] api close')

    def fire_event(self, event_name, args=None):
        """Fire event."""
        for event_func in self.events.get(event_name, []):
            if callable(event_func):
                if args is not None:
                    event_func(args)
                else:
                    event_func()
        return

    def route(self, path='/'):
        """Route decorator."""
        def _route(function):
            """Create route for function."""
            if hasattr(self, 'routes') is False:
                self.routes = dict()
            self.routes[path] = function
            return function
        return _route

    def event(self, event_name):
        """Event process method decorator."""
        def _event(function):
            """Create event for function."""
            if hasattr(self, 'events') is False:
                self.events = dict()
            if self.events.get(event_name) is None:
                self.events[event_name] = []
            self.events[event_name].append(function)
            return function
        return _event

    def add_argument(self, *args, **kwargs):
        """Add argument decorator."""
        def _add_argument(function):
            """Create argument for function."""
            _args = (args, kwargs)
            if hasattr(function, 'args'):
                function.args.append(_args)
            else:
                function.args = [_args]
            return function
        return _add_argument
