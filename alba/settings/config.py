"""
Alba configuration handler.
"""
import os
import ConfigParser
import argparse
import alba.helpers.module as module


class Config(object):
    """Alba configuration."""

    def __init__(self):
        """Apply default configuration."""
        self.current = dict({'basedir': os.path.dirname(os.path.abspath('%s/../' % __file__))})
        module.include_path('%s/externals' % self.get('basedir'))
        module.include_path(self.get('basedir'))

    def get(self, name, default_value=None):
        """Return key configuration from current or
        defaults if not exists.
        """
        return self.current.get(name, default_value)

    def set(self, name, value):
        """Set given variable to given value."""
        self.current[name] = value

    def load_file(self, config_file, update=True):
        """Load config file."""
        if os.path.isfile(config_file) is False:
            return
        c = ConfigParser.ConfigParser()
        try:
            c.read(config_file)
        except Exception as e:
            return self.add_error(e)
        config_args = dict()
        for section in c.sections():
            args = c.items(section)
            for name, value in args:
                if name == 'debug' or name == 'enable':
                    value = c.getboolean(section, name)
                if name == 'port':
                    value = c.getint(section, name)
                config_args['%s_%s' % (section, name)] = value
        if update is True:
            self.current.update(config_args)
            self.current['config_file'] = config_file
        return config_args

    def from_sysargv(self):
        """Apply configuration from argv."""
        config_file = '%s/../settings/default.ini' % self.get('basedir')
        parser = argparse.ArgumentParser(prog='alba')
        """
        parser.add_argument('--webhost', '-wh', dest='web_host',
                            default='0.0.0.0',
                            help='Web host (defaults to 0.0.0.0).')
        parser.add_argument('--webport', '-wp', dest='web_port',
                            type=int, default=5000,
                            help='Web port (defaults to 5000).')
        parser.add_argument('--debug', '-d', dest='core_debug',
                            type=bool, default=False,
                            help='Enable debug.')"""
        parser.add_argument('--config', '-c', dest='config_file',
                            default=config_file,
                            help='Configuration file. Defaults to settings/default.ini.')
        args = vars(parser.parse_args())
        config_file_args = self.load_file(args.get('config_file'),
                                          update=False)
        if config_file_args is None:
            print 'Problem with the given configuration file "%s"' % args.get('config_file')
            exit(1)
        if self.current.get('config_file') is not None:
            del args['config_file']
        self.current.update(config_file_args)
        self.current.update(args)
        return True
