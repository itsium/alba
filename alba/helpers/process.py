"""
Alba process helper.
"""
import os
import sys
import subprocess
import setproctitle


def pycall(python_script, args=[]):
    """Call given python script in an independant process.
    No parent for this process.
    python_script: Absolute path to python script.
    """
    command = [sys.executable, python_script]
    command.extend(args)
    if sys.platform.startswith('win32'):
        p = subprocess.Popen(command,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             stdin=subprocess.PIPE,
                             creationflags=0x208)
    else:
        p = subprocess.Popen(command,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             stdin=subprocess.PIPE,
                             preexec_fn=os.setsid)
    return p


def set_title(proc_title):
    """Set current process title to given string."""
    setproctitle.setproctitle(proc_title)
    return True


def get_title():
    """Get current process title."""
    return setproctitle.getproctitle()
