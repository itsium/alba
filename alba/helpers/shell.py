"""
Alba shell helper.
"""


def colorize(message, color):
    """
    Default colorize method.
    Just return message without any formating changes.
    """
    return message


def format_routes(appname, routes, colorize=colorize):
    """
    Format given routes for shell.
    """
    keys = sorted(routes.keys())
    if len(keys) == 0:
        return colorize('No routes.', 'red')
    output = ''
    output += '%sFound %d routes\n' % (colorize(appname.ljust(30), 'green'),
                                     len(keys))
    output += "".ljust(80, '-') + '\n'
    for name in keys:
        doc = routes.get(name, '')
        if len(doc) == 0:
            doc = ''
        else:
            data = doc.split('\n')
            doc = ''
            for line in data:
                line = line.strip(' \n\t')
                if len(line) > 0:
                    doc = '%s%s\n' % (doc, '%s%s' % (''.ljust(30), line.ljust(50)))
        output += '%s%s\n' % (colorize(name.ljust(30), 'green'),
                              doc.strip(' \n\t'))
    return output.strip('\n\t ')


def format_apps(apps, colorize=colorize):
    """
    Format given applications for shell.
    """
    output = ''
    keys = sorted(apps.keys())
    for name in keys:
        if len(apps[name].get('synopsis')) == 0:
            desc = 'No description.'
        else:
            desc = apps[name].get('synopsis')
        colors = ['red', 'cyan', 'yellow', 'green']
        output += '%s%s\n' % (colorize(name.ljust(30),
                                       colors[apps[name].get('status', 0)]),
                              desc.ljust(50))
    return output.strip('\n\t ')
