"""
Alba log helper.
"""
import os
import sys
import alba
import traceback


def debug(message):
    """Write given message to stderr if
    core debug is True.
    """
    if alba.config.get('core_debug') is True:
        sys.stderr.write('%s\n' % message)
        return True
    return False


def create(filename):
    """Redirect stderr to given filename."""
    folder = alba.config.get('logs_folder', '')
    if os.path.isdir(folder):
        sys.stderr = open('%s%s.log' % (folder, filename), 'a', 0)
        return True
    else:
        sys.stderr = open('/tmp/%s.log' % filename, 'a', 0)
        return True
    return False


def exception():
    """Print exception to stderr."""
    traceback.print_exc(file=sys.stderr)
