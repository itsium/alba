"""
alba arguments parser.
"""
import sys
import argparse


class AlbaArgumentParser(argparse.ArgumentParser):
    def exit(self, status=0, message=None):
        if message:
            self._print_message(message)

    def _error(self, message):
        self.print_usage(sys.stderr)

    def _print_message(self, message, file=None):
        if hasattr(self, 'error_message') is False:
            self.error_message = message


def has_args(cmd, function):
    if (cmd.get('shell', None) is None or
        hasattr(function, 'args') is False or
        type(function.args) is not list):
        return False
    return True


def check_args(cmd, function):
    if has_args(cmd, function) is False:
        return cmd.get('args', {})
    parser = AlbaArgumentParser(prog=cmd.get('app'))
    for args, kwargs in function.args:
        parser.add_argument(*args, **kwargs)
    command = cmd.get('shell').replace(cmd.get('app').replace('alba.', ''), '').strip()
    args = parser.parse_args(command.split())
    if hasattr(parser, 'error_message'):
        return parser.error_message
    return vars(args)
