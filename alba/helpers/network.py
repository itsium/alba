"""
Alba network helper.
"""
import socket


def is_running(target):
    """
    Check if given target is running.
    Return True if target is running else False.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    failed = False
    (host, port) = target.replace('tcp://', '').split(':')
    try:
        s.connect((host, int(port)))
        s.close()
    except Exception:
        failed = True
    return failed is False
