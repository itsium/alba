"""
Alba module helper.
"""
import os
import sys
import importlib


def include_path(path, position=0):
    """Include given path to python sys path."""
    if os.path.isfile(path):
        folder = os.path.dirname(os.path.abspath(path))
    else:
        folder = os.path.abspath(path)
    if folder not in sys.path:
        sys.path.insert(position, folder)
        return True
    return False


def remove_path(path):
    """Remove given path from python sys path."""
    if os.path.isfile(path):
        folder = os.path.dirname(os.path.abspath(path))
    else:
        folder = os.path.abspath(path)
    if folder in sys.path:
        sys.path.remove(folder)
        return True
    return False


def load(module_name, path):
    """Load given file path and return python module object.
    Return False if failed.
    """
    include_path(path)
    mod = None
    try:
        mod = importlib.import_module(module_name)
    except Exception as e:
        sys.stderr.write('[LOAD] Failed: %s\n' % repr(e))
    remove_path(path)
    if sys.modules.get(module_name) is not None:
        del sys.modules[module_name]
    return mod
