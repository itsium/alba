"""
Alba include.
"""
import os
import sys


def include_path():
    """
    Include alba folder to python sys path.
    """
    cmd_folder = os.path.dirname(os.path.abspath(__file__)).replace('alba/tests', '')
    if cmd_folder not in sys.path:
        sys.path.insert(0, cmd_folder)


include_path()

import alba

config_file = '%s/../settings/unittest.ini' % alba.config.get('basedir')
alba.config.load_file(config_file, update=True)
