"""
alba Hello world test case.
"""
import include
import alba
import alba.rpc.client
import alba.rpc.api.serializers as serializers
import zmq
import unittest
import threading
import Queue


class HelloTestCase(unittest.TestCase):

    def setUp(self):
        """Starting alba and creating listeners."""
        alba.start(start_shell=False)
        self.context = zmq.Context.instance()
        self.num_workers = 5
        self.num_messages = 5000

    def quit(self):
        """Clearing listeners and closing alba."""
        api = alba.rpc.client.Client()
        api.connect(alba.config.get('core_socket'), self.context)
        api.quit()

    def test_hello_thread_safe(self):
        """Test hello thread-safe."""
        workers = []
        queue = Queue.Queue()
        for i in range(0, self.num_workers):
            workers.append(threading.Thread(target=self.hello_thread_worker,
                                            args=(queue, self.context)))
            workers[i].start()
        for i in range(0, self.num_messages):
            queue.put(i)
        for i in range(0, self.num_workers):
            queue.put('quit')
        for i in range(0, self.num_workers):
            workers[i].join()
        self.quit()

    def send_hello(self, shell_command, api, listener):
        query = {'app': 'unittests.hello_world',
                 'route': 'default',
                 'args': {'name': [shell_command]}}
        command = api.execute(query)
        cid = str(command[0].get('id'))
        serializer = serializers.JSON()
        listener.setsockopt(zmq.SUBSCRIBE, cid)
        output = []
        while True:
            data = listener.recv_multipart()
            if data[1] == 'finish':
                break
            output.append(serializer.decode(data[1]))
        listener.setsockopt(zmq.UNSUBSCRIBE, cid)
        return output

    def hello_thread_worker(self, queue, context):
        """Hello thread worker."""
        api = alba.rpc.client.Client()
        api.connect(alba.config.get('core_socket'), context)
        listener = context.socket(zmq.SUB)
        listener.setsockopt(zmq.LINGER, 0)
        listener.connect(alba.config.get('core_publisher'))
        stop = False
        while stop is False:
            num = queue.get()
            if num == 'quit':
                stop = True
                continue
            result = self.send_hello('there%d' % num, api, listener)
            if result[0] == 'Hello there%d !' % num:
                res = 'SUCCESS'
            else:
                res = 'FAIL'
            print '%s -> %s' % (result[0], res)
            self.assertTrue(result[0] == 'Hello there%d !' % num)
        listener.close()
        del api
        return True


if __name__ == '__main__':
    unittest.main()
