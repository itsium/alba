"""
alba imports tests.
"""
import include
import unittest


class TestImports(unittest.TestCase):
    def test_toplevel(self):
        """
        Test toplevel import.
        """
        import alba

    def test_api(self):
        """
        Test api imports.
        """
        import alba.api
        import alba.api.process
        import alba.api.args
        import alba.api.rpc

    def test_core(self):
        """
        Test core imports.
        """
        import alba.common
        import alba.config
        import alba.shell

    def test_services_web(self):
        """
        Test services web imports.
        """
        import alba.services.web
        import alba.services.web.config


if __name__ == '__main__':
    unittest.main()
