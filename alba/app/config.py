"""
Alba application configuration.
"""
import alba.helpers.log as log


class Config(object):
    """Configuration keys and parser for
    alba applications.
    """

    def __init__(self):
        """Initialize configuration keys."""
        self.defaults = {
            'id': '',
            'name': '',
            'category': '',
            'fullname': '',
            'icon': '',
            'description': '',
            'synopsis': '',
            'socket': '',
            'fullname': '',
            'version': '',
            'process_pool': 1,
            'path': '',
            'icon': '',
            'routes': {},
            'events': {}}
        self.current = {}

    def keys(self):
        """Return all configuration keys."""
        return self.defaults.keys()

    def get(self, key, default=''):
        """Return value for given key."""
        return self.current.get(key, self.defaults.get(key, default))

    def set(self, key, value):
        """Set given value to given key."""
        self.current[key] = value

    def to_dict(self):
        """Convert configuration to dict."""
        return self.current

    def parse_from_module(self, app_config):
        """Parse application configuration dictionnary to populate
        configuration object. Used in local application
        load.
        """
        for key in self.defaults.keys():
            value = self.defaults.get(key)
            if hasattr(app_config, key):
                value = getattr(app_config, key)
            self.current[key] = value
            if hasattr(self, '_parse_%s_from_module' % key):
                parser = getattr(self, '_parse_%s_from_module' % key)
                if callable(parser):
                    self.current[key] = parser(value)
        log.debug('[CONFIG] %s' % repr(self.current))
        self.set('fullname', self.get('name'))
        if len(self.get('category')) > 0:
            self.set('fullname', '%s.%s' % (self.get('category'),
                                            self.get('name')))
        self.set('id', self.get('fullname'))
        return self.current

    def _parse_routes_from_module(self, routes):
        """Parse given routes."""
        log.debug('[ROUTES] %s' % repr(routes))
        results = {}
        for route, func in routes.iteritems():
            results[route] = func.__doc__
            if results[route] is None:
                results[route] = ''
        return results

    def parse_from_dict(self, app_config):
        """Parse application configuration dictionnary to populate
        configuration object. Used in remote application
        load.
        """
        for key in self.keys():
            default = self.get(key)
            self.set(key, app_config.get(key, default))
        self.set('fullname', self.get('name'))
        if len(self.get('category')) > 0:
            self.set('fullname', '%s.%s' % (self.get('category'),
                                            self.get('name')))
        self.set('id', self.get('fullname'))
        return self.current
