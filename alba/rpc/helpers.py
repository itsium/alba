"""
Alba RPC helpers.
"""


def rpc_method(function):
    """A decorator for use in declaring a method as an rpc method.

    Use as follows::

        @rpc_method
        def echo(self, s):
            return s
    """
    function.is_rpc_method = True
    return function
