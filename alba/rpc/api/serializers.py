"""
Alba RPC API serializers.
"""
import pickle
import zmq.utils.jsonapi
import bson


class Base(object):
    """A class for serializing/deserializing objects."""

    def encode(self, s):
        return pickle.dumps(s)

    def decode(self, o):
        return pickle.loads(o)


Pickle = Base


class JSON(Base):
    """A class for serializing using JSON."""

    def encode(self, s):
        return zmq.utils.jsonapi.dumps(s)

    def decode(self, o):
        return zmq.utils.jsonapi.loads(o)


class BSON(Base):
    """A class for serializing using BSON."""

    def encode(self, s):
        return bson.dumps(s)

    def decode(self, o):
        return bson.loads(o)
