"""
Alba RPC Base Server.
"""
import zmq
import alba.helpers.log as log
import alba.rpc.api.serializers as serializers


class BaseServer(object):
    """RPC Base server.
    No thread creation, no process creation.
    """

    def __init__(self, class_api, serializer=None):
        """Creating new mono server."""
        self.class_api = class_api
        if serializer is None:
            self.serializer = serializers.JSON()
        else:
            self.serializer = serializer
        self.context = zmq.Context.instance()
        self.socket = None
        self.may_stop = False

    def __del__(self):
        """Clean socket and context."""
        if self.socket is not None and self.socket.closed is False:
            self.socket.close()
        self.context.term()

    def bind(self, listen):
        """Bind given address to server."""
        self.listen = listen

    def start(self):
        """Start the server at given listen address."""
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind(self.listen)
        self.class_instance = self.class_api(self.context)
        while self.may_stop is False:
            message = self._recv()
            if self.handle_request(message) is False:
                return

    def stop(self):
        """Stop the server."""
        self.may_stop = True
        self.socket.close()

    def handle_request(self, message):
        """Handle given request."""
        log.debug('[API] RECV: %s' % repr(message))
        if message == 'quit':
            self.stop()
            return False
        args = message.get('args', [])
        kwargs = {}
        method = str(message.get('method', ''))
        for k, v in message.get('kwargs', {}).iteritems():
            kwargs[str(k)] = v
        result = self._call_method(method, args, kwargs)
        log.debug('[API] SEND: %s' % repr(result))
        self._send(result)
        return True

    def _send(self, message):
        """Send message to client socket."""
        data = self.serializer.encode(message)
        try:
            self.socket.send(data)
        except Exception as e:
            log.debug('[API] Send exception: %s' % repr(e))
            return False
        return True

    def _recv(self):
        """Return data received from socket."""
        data = self.socket.recv()
        if data == 'quit':
            return data
        message = self.serializer.decode(data)
        return message

    def _call_method(self, methodname, args, kwargs):
        """Call class api method and return result."""
        result = {'success': False,
                  'message': '',
                  'data': None}
        if hasattr(self.class_instance, methodname) is True:
            method = getattr(self.class_instance, methodname)
            if self._is_rpc_method(method):
                try:
                    data = method(*args, **kwargs)
                    result['data'] = data
                    result['success'] = True
                except Exception as e:
                    result['message'] = self._error_method_exception(methodname, e)
                    result['success'] = False
                    log.debug('[API] %s' % result.get('message', ''))
            else:
                result['success'] = False
        else:
            result['message'] = self._error_method_not_found(methodname)
        return result

    def _is_rpc_method(self, method):
        """Test if given method is callable from RPC."""
        if hasattr(method, 'is_rpc_method') is True:
            if getattr(method, 'is_rpc_method') is True:
                return True
        return False

    def _error_method_not_found(self, methodname):
        """Return error method not found in class API."""
        message = 'Method "%s" is not defined in "%s" class.'
        return message % (methodname, self.class_api.__name__)

    def _error_method_exception(self, methodname, e):
        """Return error exception raised while calling methodname."""
        message = 'Method "%s" raised exception: "%s"'
        return message % (methodname, str(e))
