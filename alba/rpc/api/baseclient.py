"""
Alba RPC Base Client.
"""
import zmq
import alba.helpers.log as log
import alba.rpc.api.serializers as serializers


class RPC(object):
    """RPC: zmqrpc Remote procedure call class.
    Encapsulates method calls to imported class
    """

    def __init__(self, method_name, send_function):
        """New RPC."""
        self._method_name = method_name
        self._send_function = send_function

    def __call__(self, *args, **kwargs):
        """Call client RPC class _function method to send query to server.
        Return server result.
        """
        result = self._send_function({'method': self._method_name,
                                      'args': args,
                                      'kwargs': kwargs})
        if result.get('success', False) is False:
            return
        return result.get('data')


class BaseClient(object):
    """RPC Base client.
    No thread creation, no process creation.
    """

    def __init__(self, serializer=None):
        """Create a new RPC client.
        serializer: class used for serialize/unserialize message.
        """
        if serializer is None:
            self._serializer = serializers.JSON()
        else:
            self._serializer = serializer
        self._socket = None
        self._context = None
        self._is_new_context = False

    def __del__(self):
        """Cleaning socket."""
        if self._socket is not None and self._socket.closed is False:
            self._socket.close()
        if self._is_new_context is True:
            self._context.term()

    def connect(self, listen, context=None):
        """Connect to given target.
        listen: Target to connect.
        context: (optional) zmq context to use.
        """
        if context is None:
            self._context = zmq.Context.instance()
            self._is_new_context = True
        else:
            self._context = context
        self._socket = self._context.socket(zmq.REQ)
        self._socket.setsockopt(zmq.LINGER, 0)
        self._socket.connect(listen)

    def close(self):
        """Close the connection."""
        if self._socket is not None and self._socket.closed is False:
            self._socket.close()
        if self._is_new_context is True:
            self._context.term()

    def _send(self, message):
        """Send message to server."""
        data = self._serializer.encode(message)
        try:
            self._socket.send(data)
        except Exception as e:
            log.debug('[RPC-CLIENT] Send exception: %s' % repr(e))
            return {
                'success': False,
                'message': 'Failed to send message "%s"' % data,
                'data': ''}
        rep_data = self._socket.recv()
        rep_message = self._serializer.decode(rep_data)
        return rep_message

    def _get_rpc(self, name):
        """Return new RPC class."""
        return RPC(name, self._send)

    def __getattr__(self, name):
        """Create new RPC class and return it."""
        return self._get_rpc(name)
