"""
Alba RPC Event Server.
"""
import zmq
import alba.helpers.log as log
import alba.rpc.api.baseserver as baseserver

from zmq.eventloop import ioloop, zmqstream


class EventServer(baseserver.BaseServer):
    """RPC Event server.
    Uses ZMQ eventloop.
    """

    def start(self):
        """Initializes the event loop, creates the sockets/streams and
        starts the (blocking) loop.
        """
        self.class_instance = self.class_api()
        self.loop = ioloop.IOLoop.instance()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind(self.listen)
        self.stream = zmqstream.ZMQStream(self.socket, self.loop)
        self.stream.on_recv(self.handle_request)
        self.loop.start()

    def stop(self):
        """Stop the server."""
        self.may_stop = True
        if hasattr(self, 'stream'):
            self.stream.flush()
        if hasattr(self, 'loop'):
            self.loop.stop()

    def _send(self, message):
        """Send message to client socket."""
        data = self.serializer.encode(message)
        try:
            self.stream.send(data)
        except Exception as e:
            log.debug('[API] Send exception: %s' % repr(e))
            return False
        return True

    def handle_request(self, messages):
        """Handle given request."""
        data = ''
        if type(messages) is list:
            for message in messages:
                data = '%s%s' % (data, message)
        if data != 'quit':
            data = self.serializer.decode(data)
        super(EventServer, self).handle_request(data)
