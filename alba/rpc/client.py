"""
Alba RPC Client.
"""
from alba.rpc.api.baseclient import BaseClient


class Client(BaseClient):
    """RPC Client."""

    def __init__(self, *args, **kwargs):
        self.cid = None
        BaseClient.__init__(self, *args, **kwargs)

    def use_command_id(self, cid):
        self.cid = cid

    def unuse_command_id(self, cid):
        self.cid = None

    def execute(self, commands):
        if self.cid is None:
            return self._get_rpc('execute')(commands)
        commands['id'] = self.cid
        return self._get_rpc('execute')(commands)

    def log(self, message):
        if self.cid is None:
            return self._get_rpc('log')('', message)
        return self._get_rpc('log')(self.cid, message)
