"""
Alba application entry point.
"""
import os
import alba
import time
import alba.helpers.process
import alba.helpers.network
import alba.helpers.log
import alba.ui.shell
import alba.api.rpc
import alba.rpc.server


def start(start_shell=True):
    """Start alba project."""
    alba.config.from_sysargv()
    if core_is_running() is False:
        core_path = '%s/../core.py' % os.path.dirname(__file__)
        core_path = os.path.abspath(core_path)
        alba.helpers.process.pycall(core_path,
                                    ['-c', alba.config.get('config_file')])
        # Sleep for waiting zmq socket server availability
        time.sleep(0.1)
    if start_shell is True and alba.config.get('shell_enable') is True:
        shell()


def shell():
    """Launch shell."""
    alba.config.from_sysargv()
    shell = alba.ui.shell.App()
    while shell.is_running:
        try:
            shell.cmdloop()
        except KeyboardInterrupt:
            print ''


def core():
    """Core loop function.
    Waiting for new actions to execute.
    """
    alba.config.from_sysargv()
    alba.helpers.process.set_title('[alba][core]')
    alba.helpers.log.create('alba_core')
    server = alba.rpc.server.Server(alba.api.rpc.Api)
    server.bind(alba.config.get('core_socket'))
    try:
        server.start()
    except:
        alba.helpers.log.exception()
        server.stop()
        exit(0)


def core_is_running():
    """Check if core is running.
    Return True if core is already launched, else False.
    """
    return alba.helpers.network.is_running(alba.config.get('core_socket'))
