"""
Alba bootstrap.
"""
from alba.settings.config import Config


__version__ = '0.1-dev'
config = Config()


from alba.launcher import start, shell, core
