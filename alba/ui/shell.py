"""
alba terminal.
"""
from __future__ import print_function

import os
import cmd2
import alba
import zmq
import alba.rpc.client
import alba.rpc.api.serializers as serializers
import alba.helpers.shell
import alba.helpers.log
import alba.helpers.process
import readline


class App(cmd2.Cmd):
    default_file_name = 'alba/.alba_history.txt'

    def __init__(self, *args, **kwargs):
        alba.helpers.process.set_title('[alba][shell]')
        banner = '%s/banner.txt' % os.path.dirname(__file__)
        if os.path.isfile(banner):
            print(open(banner).read())
        alba.helpers.log.create('alba_shell')
        cmd2.Cmd.__init__(self, *args, **kwargs)
        App.prompt = '%s/ ' % self.colorize('alba', 'underline')
        self.is_running = True
        self.context = zmq.Context.instance()
        self.serializer = serializers.JSON()
        self.api = alba.rpc.client.Client()
        self.api.connect(alba.config.get('core_socket'), self.context)
        self.subscriber = self.context.socket(zmq.SUB)
        self.subscriber.setsockopt(zmq.LINGER, 0)
        self.subscriber.connect(alba.config.get('core_publisher'))

    def preloop(self):
        print('')
        self._history_load(self.default_file_name)

    def postloop(self):
        self._history_save(self.default_file_name)

    def _history_save(self, filename):
        """Save history to file."""
        readline.write_history_file(filename)

    def _history_load(self, filename):
        """Load file to history."""
        if os.path.isfile(filename) is False:
            return
        readline.read_history_file(self.default_file_name)
        readline.set_history_length(1000)
        return True

    def do_exit(self, line):
        self.is_running = False
        return True

    def do_quit(self, line):
        print('quitting...')
        self.subscriber.close()
        self.api.close()
        self.context.term()
        self.is_running = False
        return True

    def do_core_quit(self, line):
        print('core quitting...')
        self.subscriber.close()
        self.api.quit()
        self.api.close()
        self.context.term()
        self.is_running = False
        return True

    def do_start(self, line):
        """usage: start appname route [args]"""
        argv = line.strip('\n\t ').split(' ')
        if len(argv) < 2:
            print(self.do_start.__doc__)
            return
        appname = argv[0]
        route = argv[1]
        cmd = {
            'app': appname,
            'route': route,
            'shell': line.replace(route, '').strip('\n\t ')}
        command = self.api.execute(cmd)
        cid = command[0].get('id')
        self.subscriber.setsockopt(zmq.SUBSCRIBE, cid)
        while True:
            output = self.subscriber.recv_multipart()
            if output[1] == 'finish':
                break
            data = self.serializer.decode(output[1])
            print(str(data))
        self.subscriber.setsockopt(zmq.UNSUBSCRIBE, cid)

    def _complete_apps(self, argument, line, begin, end):
        apps = self.api.get_all()
        results = dict()
        print('')
        for key, app in apps.iteritems():
            if len(argument) == 0 or app.get('fullname').startswith(argument) is True:
                results[app.get('fullname')] = app
        keys = sorted(results.keys())
        print(alba.helpers.shell.format_apps(results, self.colorize))
        print('%s%s' % (App.prompt, line), end='')
        return keys

    def _complete_routes(self, argument, line, begin, end):
        appname = line.split(' ')[1]
        routes = self.api.get_routes(appname)
        print('')
        results = dict()
        for routename, desc in routes.iteritems():
            if len(argument) == 0 or routename.startswith(argument) is True:
                results[routename] = desc.strip('\n\t ')
        keys = sorted(results.keys())
        self.show_routes(appname, results)
        print('%s%s' % (App.prompt, line), end='')
        return keys

    def complete_routes(self, argument, line, begin, end):
        argc = len(line.split(' '))
        if argc == 2:
            return self._complete_apps(argument, line, begin, end)
        return

    def complete_start(self, argument, line, begin, end):
        argc = len(line.split(' '))
        if argc == 2:
            return self._complete_apps(argument, line, begin, end)
        elif argc == 3:
            return self._complete_routes(argument, line, begin, end)
        return

    def show_routes(self, appname, routes):
        """Format and show routes."""
        output = alba.helpers.shell.format_routes(appname, routes, colorize=self.colorize)
        print(output)

    def do_routes(self, line):
        """Show routes from given application."""
        routes = self.api.get_routes(line)
        self.show_routes(line, routes)
