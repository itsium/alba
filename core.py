"""
Alba core.
"""
import alba


def main():
    """Start alba core."""
    alba.core()


if __name__ == '__main__':
    main()
