#!/bin/sh
ABSPATH=$(cd "$(dirname "$0")"; pwd)
pylint --ignore=nmap,externals alba/ > $ABSPATH/pylint-last-report.txt
cat $ABSPATH/pylint-last-report.txt | more
