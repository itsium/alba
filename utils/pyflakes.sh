#!/bin/sh
ABSPATH=$(cd "$(dirname "$0")"; pwd)
pyflakes services apps alba/*.py alba/api alba/app alba/ui alba/helpers alba/rpc alba/settings alba/tests alba/wrappers > $ABSPATH/pyflakes-last-report.txt
cat $ABSPATH/pyflakes-last-report.txt | more
