"""
Alba index blueprint.
"""
import subprocess
import json

from flask import Blueprint, render_template, abort, request
from jinja2 import TemplateNotFound


__terminal__ = Blueprint('terminal', __name__,
                         static_folder='static',
                         static_url_path='/static',
                         template_folder='templates',
                         url_prefix='/terminal')


@__terminal__.route('/')
def terminal_show():
    """
    Show given page if exists.
    """
    try:
        return render_template('terminal/index.html')
    except TemplateNotFound:
        abort(404)


@__terminal__.route('/exec')
def terminal_exec():
    """
    Return all available applications.
    """
    success = False
    result = 'No arguments given.'
    if request.args.get('q', None) is not None:
        proc = subprocess.Popen(request.args.get('q').split(' '),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        return_code = proc.wait()
        if return_code == 0:
            success = True
            result = proc.stdout.read()
        else:
            result = proc.stderr.read()
    result = {'data': result, 'success': success, 'id': request.args.get('id', '')}
    return json.dumps(result)
