Ext.define('App.views.appfinder.Search', {
    extend: 'Ext.container.Container',
    alias: 'widget.app.views.appfinder.search',

    initComponent: function() {
	this.layout = {
	    type: 'hbox',
	    pack: 'start',
	    align: 'stretch'
	};
	this.searchValue = '';
	this.lastSearchValue = '';
	this.items = [{
	    xtype: 'container',
	    flex: 1
	}, {
	    xtype: 'textfield',
	    flex: 1,
	    margins: '0 2 0 0',
	    ref: 'searchField',
	    enableKeyEvents: true,
	    listeners: {
		scope: this,
		keyup: this._onSearchKeyUp
	    },
	    emptyText: 'Application name or description...'
	}, {
	    xtype: 'container',
	    ref: 'searchBtnContainer',
	    html: '<input type="submit" ref="searchBtn" id="' +
		this.getId() + '-searchBtn" class="btn primary" value="Search" />',
	    width: 75
	}, {
	    xtype: 'container',
	    flex: 1
	}];
	this.on('afterrender', this._onAfterRender, this);
	this.callParent(arguments);
    },

    _onAfterRender: function(cmp) {
	this.searchBtn = Ext.get(this.getId() + '-searchBtn');
	this.searchBtn.on('click', this._onSearchBtnClicked, this);
    },

    _onSearchKeyUp: function(textfield, event, options) {
	if (event.getCharCode() === 13) {
	    this.searchValue = textfield.getValue();
	    this.searchApp();
	}
    },

    _onSearchBtnClicked: function() {
	this.searchValue = this.query('textfield[ref=searchField]')[0].getValue();
	this.searchApp();
    },

    searchApp: function() {
	if (this.searchValue.length === 0) {
	    this.store.clearFilter();
	} else {
	    this.store.filterBy(this.searchAppFilter, this);
	}
    },

    searchAppFilter: function(record, id) {
	var pattern = new RegExp(this.searchValue, 'i');
	if (record.get('name').search(pattern) !== -1) {
	    return true;
	}
    }
});