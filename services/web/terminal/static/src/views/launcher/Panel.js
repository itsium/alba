Ext.define('LauncherApp', {
    extend: 'Ext.data.Model',
    fields: [{
	name: 'id'
    }, {
	name: 'category'
    }, {
	name: 'title'
    }, {
	name: 'icon'
    }, {
	name: 'nb_updates'
    }, {
	name: 'is_active'
    }],
    propertyId: 'id'
});

Ext.define('App.models.LauncherStore', {
    extend: 'Ext.data.Store',
    widget: 'widget.app.models.actionstore',

    constructor: function(config) {
	/*this.reader = {
	    type: 'json',
	    root: 'results'
	};*/
	this.model = 'LauncherApp';
	this.sorters = [{
	    property: 'category',
	    direction: 'ASC'
	}];
	this.callParent(arguments);
	this.loadData([{
	    id: 'appfinder',
	    category: 'services',
	    title: 'AppFinder',
	    icon: '',
	    nb_updates: 0,
	    is_active: true
	}, {
	    id: 'dashboard',
	    category: 'services',
	    title: 'Dashboard',
	    icon: '',
	    nb_updates: 0,
	    is_active: false
	}, {
	    id: 'fake',
	    category: 'fake',
	    title: 'Fake',
	    icon: '',
	    nb_updates: 0,
	    is_active: false
	}], false);
    }
});

Ext.define('App.views.launcher.Panel', {
    extend: 'App.controllers.AccordionMenu',
    requires: [
	'App.controllers.AccordionMenu'
    ],
    alias: 'widget.app.views.launcher.panel',

    initComponent: function() {
	this.store = Ext.create('App.models.LauncherStore', {});
	this.cls = 'launcher-category-container';
	this.autoScroll = true;
	this.callParent(arguments);
    }
});
