"""
alba args helpers.
"""
import pymongo


def extjs_filters_to_pymongo(filters):
    """Transform extjs filters args to readable pymongo criteria args."""
    result = dict()
    for filter in filters:
        if filter.get('property') is not None and \
                filter.get('value') is not None:
            result[filter.get('property')] = filter.get('value')
    return result


def extjs_sorts_to_pymongo(sorts):
    """Transform extjs sorters args to readable pymongo sort args."""
    result = []
    for sort in sorts:
        direction = pymongo.ASCENDING
        if sort.get('property') is not None:
            if sort.get('direction', 'ASC') == 'DESC':
                direction = pymongo.DESCENDING
            result.append([sort.get('property'), direction])
    return result
