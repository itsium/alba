"""
alba database helper.
"""
import bson


class Collection(object):
    """
    Collection helper to generate add/edit/get/list/remove methods
    from given database and collection name.
    """
    def __init__(self, db, collection_name, includes={}):
        """new Collection(__db__, 'test')"""
        self.name = collection_name
        self.db = db
        self.includes = includes

    def get_db(self, name=None):
        """Return collection."""
        if name is None:
            return getattr(self.db, self.name)
        else:
            return getattr(self.db, name)

    def apply_includes(self, record):
        """Apply includes to record."""
        for foreign_key, dbname in self.includes.iteritems():
            name = foreign_key.replace('_id', '')
            db = self.get_db(dbname)
            record[name] = db.find_one({'id': record.get(foreign_key, '')})
            if record.get(name, None) is not None:
                del record.get(name)['_id']
        return record

    def add(self, data):
        """Add data to collection."""
        data['id'] = str(bson.objectid.ObjectId())
        self.get_db().insert(data)
        return self.get(data.get('id'))

    def get(self, record_id):
        """Get record from given id."""
        record = self.get_db().find_one({'id': record_id})
        del record['_id']
        record = self.apply_includes(record)
        return record

    def edit(self, record_id, data):
        """Edit given record with data from given id."""
        if data.get('id', None) is not None:
            del data['id']
        self.get_db().update({'id': record_id}, {'$set': data}, True)
        return self.get(record_id)

    def is_valid_sort(self, sort):
        """Test if given sort is valid."""
        if sort is not None and ((type(sort) is str and len(sort) > 0) or
                                 (type(sort) is list and len(sort) > 0)):
            return True
        return False

    def is_valid_skip(self, skip):
        """Test if given skip is valid."""
        if skip is not None and type(skip) is int and skip > -1:
            return True
        return False

    def is_valid_limit(self, limit):
        """Test if given limit is valid."""
        if limit is not None and type(limit) is int and limit > -1:
            return True
        return False

    def search(self, criteria={}, sort=None, skip=None, limit=None):
        """Search records."""
        cursor = self.get_db().find(criteria)
        if self.is_valid_sort(sort) is True:
            cursor = cursor.sort(sort)
        if self.is_valid_skip(skip) is True:
            cursor = cursor.skip(skip)
        if self.is_valid_limit(limit) is True:
            cursor = cursor.limit(limit)
        records = []
        for record in cursor:
            del record['_id']
            records.append(self.apply_includes(record))
        return records

    def remove(self, record_id):
        """Delete record from given id."""
        return self.get_db().remove({'id': record_id})
