Ext.define('Common.Settings', {
    singleton: true,
    extend: 'Ext.util.Observable',
    requires: [
	'Ext.util.Observable'
    ],

    constructor: function() {
	this.vars = {};
	this.addEvents({
	    /**
	     * Fires when settings changed.
	     *
	     * @event settingchanged
	     * @param {Object} obj This object.
	     * @param {String} name Setting name.
	     * @param {Mixed} value Setting new value.
	     * @param {Mixed} oldValue Setting old value.
	     */
	    settingchanged: true
	});
	this.callParent(arguments);
    },

    set: function(name, value) {
	var oldValue = this.get(name);
	this.vars[name] = value;
	this.fireEvent('settingchanged', this, name, value, oldValue);
	this.fireEvent(name + 'changed', this, name, value, oldValue);
    },

    get: function(name, defaultValue) {
	if (Ext.isDefined(this.vars[name])) {
	    return this.vars[name];
	}
	return defaultValue;
    }
});