Ext.define('Common.controllers.JsonNested', {
    extend: 'Ext.data.writer.Json',
    requires: [
	'Ext.data.writer.Json'
    ],
    alias: 'writer.json.nested',

    /**
     * This function overrides the default implementation of
     * json writer. Any hasMany relationships will be submitted
     * as nested objects
     */
    getRecordData: function(record) {
        var me = this, i, association, childStore, data = {};
        data = this.callParent(arguments);

        /* Iterate over all the hasMany associations */
        for (i = 0; i < record.associations.length; i++) {
            association = record.associations.get(i);
            if (association.type == 'hasMany')  {
                data[association.name] = [];
                childStore = record[association.name]();

                //Iterate over all the children in the current association
                childStore.each(function(childRecord) {

                    //Recursively get the record data for children (depth first)
                    var childData = this.getRecordData.call(this, childRecord);
                    if (childRecord.dirty | childRecord.phantom | (childData != null)){
                        data[association.name].push(childData);
                        record.setDirty();
                    }
                }, me);
            }
        }
        return data;
    }
});