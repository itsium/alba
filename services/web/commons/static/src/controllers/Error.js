Ext.define('Common.controllers.Error', {
    extend: 'Ext.util.Observable',
    requires: [
	'Ext.util.Observable',
	'Ext.data.JsonP'
    ],
    singleton: true,
    config: {
	/**
	 * @cfg {Boolean} userAlert
	 * True to enable messageBox when error is raised.
	 * False to disable it.
	 * Defaults to true.
	 */
	userAlert: true,

	/**
	 * @cfg {String} errorMsg
	 * Custom error message for user alert message box.
	 * Defauts to undefined (generated).
	 */
	errorMsg: undefined,

	/**
	 * @cfg {String} url
	 * Url to call via script tag proxy with error details.
	 * Defaults to undefined (disabled).
	 */
	url: undefined,

	/**
	 * @cfg {Object} extraParams
	 * One or several params send to config url when error is raised.
	 * Empty object to disable it.
	 * Defaults to empty object.
	 */
	extraParams: {},

	/**
	 * @cfg {Function} handler
	 * Handler function called when error is raised.
	 * Given arguments are:
	 * <ul>
	 *  <li><b>obj: </b> This object</li>
	 *  <li><b>msg: </b> Error message</li>
	 *  <li><b>stacktrace: </b> Stacktrace</li>
	 * </ul>
	 * Defaults to undefined.
	 */
	handler: undefined,

	/**
	 * @cfg {Object} scope
	 * Scope for handler.
	 * Defaults to undefined.
	 */
	scope: undefined
    },

    constructor: function(config) {
	this.addEvents({
	    /**
	     * @event beforeerror
	     * Fires before error is treated.
	     * @param {Ext.ux.Error} obj This object.
	     * @param {String} msg Error message.
	     * @param {String} date Date.
	     */
	    beforeerror: true,
	    /**
	     * @event error
	     * Fires after an error is raised.
	     * @param {Ext.ux.Error} obj This object.
	     * @param {String} msg Error message.
	     * @param {String} date Date.
	     */
	    error: true
	});
	Ext.apply(this, config);
	window.onerror = Ext.bind(this._onErrorRaised, this);
	this.callParent(arguments);
    },

    // private
    _onErrorRaised: function(msg, filename, linenumber) {
	var date = new Date().toGMTString();
	var msg = "Error: \n - Date: " + date + "\n" +
	    " - Filename: " + filename + "\n" +
	    " - Line: " + linenumber + "\n" +
	    " - Message: " + msg + "\n";
	if (this.fireEvent('beforeerror', this, msg, date) !== true) {
	    return false;
	}
	this._sendRequest(msg, date);
	this._callHandler(msg, date);
	this._showUserAlert(msg, date);
	this.fireEvent('error', this, msg, date);
	return true;
    },

    // private
    _sendRequest: function(msg, date) {
	if (Ext.isDefined(this.url)) {
	    Ext.data.JsonP.request(this.url, {
		callbackKey: 'jsoncallback',
		params: Ext.apply(this.extraParams, {
		    msg: msg,
		    date: date
		})
	    });
	}
    },

    // private
    _callHandler: function(msg, date) {
	if (Ext.isFunction(this.handler)) {
	    if (Ext.isObject(this.scope) === false) {
		this.scope = this;
	    }
	    this.handler.call(this.scope, this, msg, date);
	    return true;
	}
	return false;
    },

    // private
    _showUserAlert: function(msg, date) {
	if (this.userAlert === true) {
	    if (Ext.isDefined(this.errorMsg) === false) {
		this.errorMsg = 'An error occured.'
		if (Ext.isDefined(this.url)) {
		    this.errorMsg += ' A request was sent to administrators about this.';
		}
	    }
	    Ext.Msg.show({
		title: 'Error',
		msg: this.errorMsg,
		buttons: Ext.Msg.OK,
		icon: Ext.MessageBox.ERROR
	    });
	    return true;
	}
	return false;
    }
});