Ext.define('Common.controllers.StatusComboModel', {
    extend: 'Ext.data.Model',
    fields: [{
	name: 'text'
    }, {
	name: 'value'
    }]
});

Ext.define('Common.controllers.StatusCombo', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
	'Ext.form.field.ComboBox'
    ],
    alias: 'widget.app.controllers.statuscombo',

    initComponent: function() {
	this.store = Ext.create('Ext.data.Store', {
	    model: 'Common.controllers.StatusComboModel',
	    proxy: {
		type: 'memory',
		reader: {
		    type: 'json',
		    root: 'status'
		}
	    },
	    data: {
		status: [{
		    text: 'En attente de paiement',
		    value: 'not_paid_yet'
		}, {
		    text: 'Payée',
		    value: 'paid'
		}]
	    }
	});
	this.minChars = 1;
	this.displayField = 'text';
	this.valueField = 'value';
	this.emptyText = 'Sélectionnez un status...';
	this.callParent(arguments);
    }
});