Ext.define('Common.controllers.LoadingButton', {
    extend: 'Ext.button.Button',
    requires: [
	'Ext.button.Button'
    ],
    alias: 'widget.app.controllers.loadingbutton',

    initComponent: function() {
	if (Ext.isString(this.loadingIconCls) === false) {
	    this.loadingIconCls = 'icon-loading';
	}
	if (Ext.isString(this.loadingText) === false) {
	    this.loadingText = t('Loading') + '...';
	}
	if (Ext.isString(this.iconCls)) {
	    this.initialIconCls = this.iconCls;
	} else {
	    this.initialIconCls = '';
	}
	if (Ext.isString(this.text)) {
	    this.initialText = this.text;
	} else {
	    this.initialText = '';
	}
	this.callParent(arguments);
    },

    setDisabled: function(disabled) {
	if (disabled === true) {
	    this.setText(this.loadingText);
	    this.setIconCls(this.loadingIconCls);
	} else {
	    this.setText(this.initialText);
	    this.setIconCls(this.initialIconCls);
	}
	this.callParent(arguments);
    }
});