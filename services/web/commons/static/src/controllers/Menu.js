Ext.define('Common.controllers.MenuModel', {
    extend: 'Ext.data.Model',
    requires: [
	'Ext.data.Model'
    ],
    fields: [{
	name: 'iconCls',
	type: 'string'
    }, {
	name: 'text',
	type: 'string'
    }, {
	name: 'itemId',
	type: 'string'
    }, {
	name: 'callback'
    }, {
	name: 'scope'
    }]
});

Ext.define('Common.controllers.MenuStore', {
    extend: 'Ext.data.Store',
    requires: [
	'Ext.data.Store'
    ],

    constructor: function() {
	this.model = 'Common.controllers.MenuModel';
	this.callParent(arguments);
    }
});

Ext.define('Common.controllers.Menu', {
    extend: 'Ext.panel.Panel',
    requires: [
	'Ext.panel.Panel',
	'Ext.view.View',
	'Ext.util.CSS'
    ],
    alias: 'widget.app.controllers.menu',

    initComponent: function() {
	this.addCSS();
	this.items = [{
	    xtype: 'dataview',
	    tpl: new Ext.XTemplate(
		'<tpl for=".">',
		' <div class="{iconCls} menu-item">',
		'  {text}',
		' </div>',
		'</tpl>'
	    ),
	    store: Ext.create('Common.controllers.MenuStore', {
		data: this.menuData
	    }),
	    itemSelector: 'div.menu-item',
	    selectedItemCls: 'menu-item-selected',
	    singleSelect: true,
	    listeners: {
		scope: this,
		beforeitemclick: this._onBeforeitemclickFired
	    }
	}];
	this.on('afterrender', this._onAfterrenderFired, this);
	this.callParent(arguments);
    },

    _onBeforeitemclickFired: function(view, record) {
	if (Ext.isFunction(record.get('callback'))) {
	    var scope = view;
	    if (Ext.isObject(record.get('scope'))) {
		scope = record.get('scope');
	    }
	    if (record.get('callback').call(scope, record) === false) {
		return false;
	    }
	}
    },

    _onAfterrenderFired: function() {
	this.getComponent(0).enableBubble('itemclick');
	this.getComponent(0).select(0);
    },

    addCSS: function() {
	Ext.util.CSS.createStyleSheet([
	    '.menu-item {',
	    ' width: 100%;',
	    ' height: 25px;',
	    ' color: rgba(0, 0, 0, 0.7);',
	    ' text-shadow: 0px 1px rgba(255, 255, 255, 0.8);',
	    ' line-height: 21px;',
	    ' border: 1px solid transparent;',
	    ' padding: 2px 2px 2px 23px;',
	    ' background-position: 2px center !important;',
	    ' border-radius: 3px;',
	    '}',
	    '.menu-item:hover {',
	    ' cursor: pointer;',
	    ' background-color: rgba(255, 255, 255, 0.3);',
	    '}',
	    '.menu-item-selected {',
	    ' font-weight: bold;',
	    ' background-color: rgba(0, 0, 0, 0.1);',
	    ' text-shadow: 0px 1px rgba(255, 255, 255, 0.6);',
	    '}'
	].join(''), 'app-controllers-menu');
    }
});