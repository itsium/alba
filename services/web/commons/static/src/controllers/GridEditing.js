Ext.define('Common.controllers.GridEditing', {
    extend: 'Ext.grid.Panel',
    requires: [
	'Common.controllers.I18N',
	'Ext.grid.plugin.RowEditing',
	'Ext.grid.Panel'
    ],
    alias: [
	'widget.app.controllers.gridediting'
    ],

    initComponent: function() {
	this.addEvents({
	    /**
	     * Fires before user wanted to add a new record.
	     * Return false to cancel add.
	     * @event beforeaddrecord
	     * @param {Object} obj This object.
	     * @param {Model} record New record.
	     * @param {Store} store The store.
	     */
	    beforeaddrecord: true,
	    /**
	     * Fires after user edited record.
	     * @event aftereditrecord
	     * @param {Object} obj This object.
	     * @param {Store} store The store
	     * @param {Model} record The record.
	     */
	    aftereditrecord: true,
	    /**
	     * Fires before user wanted to delete a record.
	     * Return false to cancel remove.
	     * @event beforedeleterecord
	     * @param {Object} obj This object.
	     * @param {Model} record Record to delete.
	     * @param {Store} store The store.
	     */
	    beforedeleterecord: true,
	    /**
	     * Fires after user delete a record.
	     * @event afterdeleterecord
	     * @param {Object} obj This object.
	     * @param {Model} record Record to delete.
	     * @param {Store} store The store.
	     */
	    afterdeleterecord: true
	});
	this.storeLoaded = false;
	if (Ext.isDefined(this.autoSync) === false) {
	    this.autoSync = true;
	}
	this.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
	    errorSummary: false
	});
	this.plugins = [
	    this.rowEditing
	];
	if (Ext.isDefined(this.addPagingToolbar) === false) {
	    this.addPagingToolbar = true;
	}
	if (Ext.isDefined(this.minRecordsRequired) === false) {
	    this.minRecordsRequired = -1;
	}
	this.dockedItems = [{
	    xtype: 'toolbar',
	    dock: 'top',
            items: [{
                text: t('Add'),
                iconCls: 'icon-add-small',
		itemId: 'add-btn',
		scope: this,
                handler: this._addNewRecord
            }, '-', {
                itemId: 'delete',
                text: t('Delete'),
                iconCls: 'icon-remove-small',
		itemId: 'delete-btn',
                disabled: true,
		scope: this,
                handler: this._deleteBtnClicked
	    }]
	}];
	if (this.addPagingToolbar === true) {
	    this.dockedItems.push({
		xtype: 'pagingtoolbar',
		store: this.store,
		dock: 'bottom',
		displayInfo: true
	    });
	}
	this.store.on('beforeload', this._onStoreBeforeloadFired, this);
	this.on('afterrender', this._onAfterRenderFired, this);
	this.on('activate', this._onActivateFired, this);
	this.on('edit', this._onEditFired, this);
	this.callParent(arguments);
    },

    _onEditFired: function(editor, e, eOpts) {
	if (this.autoSync === true) {
	    this.store.sync({
		scope: this,
		callback: function(batch, options) {
		    this.fireEvent('aftereditrecord', this, this.store, e.record);
		}
	    });
	}
    },

    _deleteBtnClicked: function() {
	Ext.Msg.show({
	    title: t('Delete') + ' ?',
	    msg: t('Do you really want to delete this data') + ' ?',
	    buttons: Ext.Msg.YESNO,
	    icon: 'icon-large-question',
	    scope: this,
	    fn: this._onDeleteConfirmed
	});
    },

    _onDeleteConfirmed: function(buttonId) {
	if (buttonId !== 'yes') {
	    return false;
	}
	this._deleteCurrentRecord();
    },

    _addNewRecord: function() {
	var defaultData = {};
	if (Ext.isObject(this.defaultRecordData)) {
	    defaultData = this.defaultRecordData;
	}
	var record = new (this.store.getProxy().getModel())(defaultData);
	if (this.fireEvent('beforeaddrecord', this, record, this.store) === false) {
	    return false;
	}
        this.store.insert(0, record);
        this.rowEditing.startEdit(0, 0);
    },

    _deleteCurrentRecord: function() {
        var selection = this.getView().getSelectionModel().getSelection()[0];
        if (selection) {
	    if (this.fireEvent('beforedeleterecord', this, selection, this.store) === false) {
		return false;
	    }
            this.store.remove(selection);
	    if (this.autoSync === true) {
		this.store.sync({
		    scope: this,
		    callback: function(batch, options) {
			this.fireEvent('afterdeleterecord', this, selection, this.store);
		    }
		});
	    }
        }
    },

    _loadStore: function() {
	if (this.autoLoadStore !== false &&
	    this.storeLoaded === false) {
	    this.store.load();
	} else if (this.needRefresh === true) {
	    this.store.load();
	}
    },

    _onStoreBeforeloadFired: function() {
	this.storeLoaded = true;
	this.needRefresh = false;
    },

    _onAfterRenderFired: function() {
	this._loadStore();
	this.tb = this.getDockedItems('toolbar[dock="top"]')[0];
	this.getSelectionModel().on('selectionchange', function(selModel, selections) {
	    var deleteBtn = this.tb.query('button[itemId="delete-btn"]')[0];
	    if (this.minRecordsRequired !== -1) {
		deleteBtn.setDisabled(this.getStore().getCount() <= this.minRecordsRequired);
	    } else {
		deleteBtn.setDisabled(selections.length === 0);
	    }
	}, this);
    },

    _onActivateFired: function() {
	this._loadStore();
    }
});