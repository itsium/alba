Ext.define('Common.controllers.SimpleGrid', {
    extend: 'Ext.grid.Panel',
    requires: [
	'Ext.grid.Panel'
    ],
    alias: [
	'widget.app.controllers.simplegrid'
    ],

    initComponent: function() {
	this.storeLoaded = false;
	if (Ext.isArray(this.dockedItems) === false &&
	    Ext.isObject(this.dockedItems) === false) {
	    this.dockedItems = [];
	} else if (Ext.isObject(this.dockedItems) === true) {
	    this.dockedItems = [this.dockedItems];
	}
	this.dockedItems.push({
	    xtype: 'pagingtoolbar',
            store: this.store,
            dock: 'bottom',
            displayInfo: true
	});
	this.store.on('beforeload', this._onStoreBeforeloadFired, this);
	this.on('afterrender', this._onAfterRenderFired, this);
	this.on('activate', this._onActivateFired, this);
	this.callParent(arguments);
    },

    _loadStore: function() {
	if (this.autoLoadStore !== false &&
	    this.storeLoaded === false) {
	    this.store.load();
	} else if (this.needRefresh === true) {
	    this.store.load();
	}
    },

    _onStoreBeforeloadFired: function() {
	this.storeLoaded = true;
	this.needRefresh = false;
    },

    _onAfterRenderFired: function() {
	this._loadStore();
    },

    _onActivateFired: function() {
	this._loadStore();
    }
});