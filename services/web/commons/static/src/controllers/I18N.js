Ext.define('Common.controllers.I18N', {
    singleton: true,
    extend: 'Ext.util.Observable',
    requires: [
	'Ext.util.Observable'
    ],

    constructor: function() {
	this.lang = 'en';
	this.path = '/javascripts/src/lang/';
	this.extPath = '/javascripts/extjs/';
	this.langs = {
	    en: {
		name: 'English',
		value: 'en',
		file: 'en.js',
		extFile: 'locale/ext-lang-en.js',
		iconCls: 'icon-lang-en'
	    },
	    fr: {
		name: 'French',
		value: 'fr',
		file: 'fr.js',
		extFile: 'locale/ext-lang-fr.js',
		iconCls: 'icon-lang-fr'
	    },
	    'zh-CN': {
		name: 'Chinese',
		value: 'zh-CN',
		file: 'cn.js',
		extFile: 'locale/ext-lang-zh_CN.js',
		iconCls: 'icon-lang-cn'
	    },
	    it: {
		name: 'Italian',
		value: 'it',
		file: 'it.js',
		extFile: 'locale/ext-lang-it.js',
		iconCls: 'icon-lang-it'
	    }
	};
	this.addEvents({
	    /**
	     * @event langloaded
	     *
	     * @param {Object} obj This object.
	     * @param {String} lang Language text.
	     * @param {Object} langObj Lang object.
	     */
	    langloaded: true
	});
	this.callParent(arguments);
    },

    getLangs: function() {
	return this.langs;
    },

    setLang: function(lang) {
	if (Ext.isObject(this.langs[lang]) === true) {
	    this.lang = lang;
	    Ext.Ajax.request({
		url: this.extPath + this.langs[lang].extFile,
		scope: this,
		success: function(response, opts) {
		    eval(response.responseText);
		}
	    });
	    if (Ext.isObject(this[lang]) === false) {
		Ext.Ajax.request({
		    url: this.path + this.langs[lang].file,
		    scope: this,
		    success: function(response, opts) {
			this[this.lang] = eval('(' + response.responseText + ')');
			if (Ext.grid.RowEditor) {
			    Ext.apply(Ext.grid.RowEditor.prototype, {
				saveBtnText: t('Save'),
				cancelBtnText: t('Cancel'),
				errorsText: t('Errors'),
				dirtyText: t('You need to commit or cancel your changes')
			    });
			}
			this.fireEvent('langloaded', this, this.lang, this[this.lang]);
		    }
		});
	    }
	} else {
	    this.setLang('en');
	}
    },

    getCurrentLang: function() {
	if (Ext.isObject(this[this.lang]) === true) {
	    return this[this.lang];
	}
	return this.en;
    },

    find: function(text) {
	if (Ext.isString(this.getCurrentLang()[text]) === true) {
	    return this.getCurrentLang()[text];
	}
	return text;
    }
});

t = function(text) {
    return Common.controllers.I18N.find(text);
}