Ext.define('Common.Session', {
    singleton: true,
    extend: 'Ext.util.Observable',
    requires: [
	'Ext.util.Observable'
    ],

    constructor: function() {
	this.vars = {};
	this.addEvents({
	    /**
	     * Fires when variable value changed.
	     *
	     * @event varchange
	     * @param {Object} obj This object.
	     * @param {String} name Variable name.
	     * @param {Mixed} value Variable new value.
	     * @param {Mixed} oldValue Variable old value.
	     */
	    varchange: true
	    /**
	     * Fires when specific variable value changed.
	     *
	     * @event {varname}change
	     * @param {Object} obj This object.
	     * @param {Mixed} value Variable new value.
	     * @param {Mixed} oldValue Variable old value.
	     */
	});
	this.callParent(arguments);
    },

    set: function(name, value) {
	var oldValue = this.get(name);
	this.vars[name] = value;
	this.fireEvent('varchange', this, name, value, oldValue);
	this.fireEvent(name + 'change', this, value, oldValue);
    },

    get: function(name, defaultValue) {
	if (Ext.isDefined(this.vars[name])) {
	    return this.vars[name];
	}
	return defaultValue;
    }
});