"""
Alba commons blueprint.
"""
from flask import Blueprint


__commons__ = Blueprint('commons', __name__,
                        static_folder='static',
                        static_url_path='/static',
                        template_folder='templates',
                        url_prefix='/commons')
