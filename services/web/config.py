"""
alba web service config.
"""
import os
import sys
sys.path.insert(0, os.path.abspath('%s/../..' % os.path.dirname(os.path.abspath(__file__))))

import alba
import importlib
import urllib2
import werkzeug.serving
import alba.api.process

from flask import request
from common import __app__


class Service(alba.api.process.App):
    """Configuration for web service."""
    name = 'web'
    category = 'services'
    description = 'Web service'
    synopsis = 'Provide HTTP server for UI.'
    process_pool = 2
    socket = 'tcp://127.0.0.1:50002'


app = Service(__name__, __file__)


@__app__.route('/shutdown')
def web_shutdown():
    """
    Route to shutdown web server.
    """
    shutdown_server(request.environ)
    return 'Server shutting down...'


@__app__.after_request
def after_request(response):
    """
    After request event.
    """
    data = '[alba][services][web] %s | %s "%s"' % (response.status,
                                                   request.method,
                                                   request.url)
    # @TODO replace by __app__.api (RPC call)
    #alba.action.put({'app': 'alba.services.logging',
    #                 'action': 'log',
    #                 'args': data})
    return response


@app.route('default')
def web_start(api, args=None):
    """
    Start HTTP server.
    Host is from argument --webhost.
    Port is from argument --webport.
    """
    api.execute({'app': 'alba.services.logging',
                 'action': 'log',
                 'args': {'message': 'TEST'}})
    if api.config_get('is_running') is True:
        return False
    for modid, module in api.config_get('modules', dict()).iteritems():
        __app__.register_blueprint(get_module(module))
    api.config_set('is_running', True)
    try:
        werkzeug.serving.run_simple(hostname=api.config_get('web_host'),
                                    port=api.config_get('web_port'),
                                    application=__app__)
    except Exception as e:
        print repr(e)
        api.config_set('is_running', False)
    api.config_set('is_running', False)
    return


def shutdown_server(environ):
    """
    Called when web server must be shutdown.
    """
    if not 'werkzeug.server.shutdown' in environ:
        raise RuntimeError('Not running the development server')
    environ['werkzeug.server.shutdown']()


@app.route('stop')
def web_stop(api, args=None):
    """
    Stop web server.
    """
    if api.config_get('is_running') is True:
        try:
            f = urllib2.urlopen('http://127.0.0.1:%d/shutdown' % api.config_get('webport'))
        except Exception as e:
            print repr(e)
            return False
        return f.read()
    return False


@app.route('restart')
def web_restart(api, args=None):
    """
    Restart flask app.
    """
    if api.config_get('is_running') is True:
        api.execute([{'app': 'alba.services.web', 'action': 'stop'},
                     {'app': 'alba.services.web'}])
    else:
        api.execute({'app': 'alba.services.web'})
    return True


def get_module(args):
    """
    Get modules.
    """
    try:
        result = importlib.import_module('%s' % args.get('import'))
    except ImportError as e:
        print repr(e)
        return False
    if result is None or hasattr(result, args.get('name')) is False:
        return False
    try:
        return getattr(result, args.get('name'))
    except Exception as e:
        print repr(e)
        return False
    return True


@app.route('add_module')
def add_module(api, args):
    """
    Add given module to web server.
    """
    try:
        result = importlib.import_module('%s' % args.get('import'))
    except ImportError as e:
        print repr(e)
        return False
    if result is None or hasattr(result, args.get('name')) is False:
        return False
    modules = api.config_get('modules', dict())
    modid = '%s/%s' % (args.get('import'), args.get('name'))
    try:
        modules[modid] = args#getattr(result, args.get('name'))
        api.config_set('modules', modules)
        api.execute({'app': 'alba.services.web', 'action': 'restart'})
    except Exception as e:
        print repr(e)
        return False
    return True


@app.route('remove_module')
def remove_module(api, args):
    """
    Remove given module from web server.
    """
    modules = api.config_get('modules', dict())
    modid = '%s/%s' % (args.get('import'), args.get('name'))
    if modules.get(modid) is not None:
        del modules[modid]
        api.config_set('modules', modules)
        api.execute({'app': 'alba.services.web', 'action': 'restart'})
    return


if __name__ == '__main__':
    if len(sys.argv) > 1:
        app.boot(sys.argv[1])
