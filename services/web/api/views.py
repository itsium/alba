"""
Web HTTP server api.
"""
import os
import json
import alba.api.app

from flask import Blueprint, make_response


__api__ = Blueprint('api', __name__,
                    url_prefix='/api')


@__api__.route('/app/list')
def app_list():
    """
    List all available applications.
    """
    apps = alba.api.app.listall()
    return json.dumps(apps)


@__api__.route('/app/get/<appid>')
def app_get(appid):
    """
    Return application description from given app id.
    """
    app = alba.api.app.get(appid)
    if app is not None:
        return json.dumps(app.to_dict())
    return 'Not found'


def app_icon_default():
    """
    Return default application icon.
    """
    data = open('%s/%s' % (os.path.dirname(__file__), 'icon.png')).read()
    response = make_response(data)
    response.headers['Content-type'] = 'image/png'
    return response


@__api__.route('/app/icon/<appid>')
def app_icon_get(appid):
    """
    Return icon from given app id.
    """
    app = alba.api.app.get(appid)
    if app is None or app.has_icon() == False:
        return app_icon_default()
    response = make_response(open(app.get_icon()).read())
    response.headers['Content-type'] = 'image/png'
    return response
