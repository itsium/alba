Ext.define('App.controllers.AccordionMenu', {
    extend: 'Ext.container.Container',
    alias: 'widget.app.controllers.accordionmenu',

    initComponent: function() {
	this.store.on('datachanged', this._onStoreDataChanged, this);
	this.store.on('load', this._onStoreLoad, this);
	this.on('afterrender', this._onAfterRender, this);
	this.layout = {
	    type: 'accordion',
	    hideCollapseTool: true,
	    titleCollapse: true,
	    animate: true,
	    multi: true
	};
	this.ref = {};
	this.width = 240;
	this.callParent(arguments);
    },

    _onStoreDataChanged: function(store, options) {
	this.reloadGroups();
    },

    _onStoreLoad: function(store, records, success, operation, options) {
	this.reloadGroups();
    },

    _onAfterRender: function(cmp) {
	this.reloadGroups();
    },

    reloadGroups: function() {
	var records = this.store.getRange();
	var length = records.length;
	if (Ext.isArray(records) === false ||
	    length < 1) {
	    return false;
	}
	for (var i = 0; i < length; ++i) {
	    if (this.hasGroup(records[i].get('category')) !== true) {
		this.addGroup(records[i].get('category'));
	    }
	}
	return true;
    },

    hasGroup: function(name) {
	if (Ext.isObject(this.ref[name])) {
	    return true;
	}
	return false;
    },

    getGroup: function(name) {
	return this.ref[name];
    },

    addGroup: function(name) {
	this.ref[name] = this.add({
	    xtype: 'app.controllers.accordionsubmenu',
	    name: name,
	    store: this.store,
	    listeners: {
		afterrender: function(cmp) {
		    cmp.expand();
		}
	    }
	});
    }
});

Ext.define('App.controllers.AccordionSubMenu', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.app.controllers.accordionsubmenu',

    initComponent: function() {
	this.addEvents({
	    /**
	     * Fires when selection changed.
	     * @event tabchange
	     * @param {Object} record Selected record
	     */
	    tabchange: true
	});
	this.title = this.name;
	this.cls = 'accordionsubmenu-container';
	this.iconCls = 'icon-' + this.name;
	this.layout = 'fit';
	this.autoScroll = true;
	this.items = [{
	    xtype: 'dataview',
	    store: this.store,
	    tpl: Ext.create('Ext.XTemplate',
			    '<tpl for=".">',
			    ' <tpl if="category == \'', this.name, '\'">',
			    '  <div class="accordionsubmenu-app-item',
			    '   <tpl if="is_active == true"> is_active</tpl>">',
			    '   <div class="arrow_right"></div>',
			    '   <div class="accordionsubmenu-app-title">',
			    '    {title}',
			    '   </div>',
			    '   <div class="accordionsubmenu-app-updates',
			    '    <tpl if="nb_updates &lt; 1"> hidden</tpl>">',
			    '    <tpl if="nb_updates &gt; 99">99+</tpl>',
			    '    <tpl if="nb_updates &lt; 100">{nb_updates}</tpl>',
			    '   </div>',
			    '  </div>',
			    '  <div style="clear: both;"></div>',
			    ' </tpl>',
			    '</tpl>'),
	    itemSelector: 'div.accordionsubmenu-app-item',
	    singleSelect: true,
	    autoScroll: true,
	    trackOver: true,
	    listeners: {
		scope: this,
		//selectionchange: this._onSelectionChange,
		itemclick: this._onItemClick
	    }
	}];
	this.callParent(arguments);
    },

    _onItemClick: function(view, record, item, index, event, eventOptions) {
	console.log('itemclick: ', record.get('title'), item);
	var previous = this.store.findRecord('is_active', true);
	console.log('previous: ', previous.get('title'));
	previous.set('is_active', false);
	this.record = record;
	this.record.set('is_active', true);
	console.log('current: ', this.record.get('title'));
    },

    _onSelectionChange: function(view, selections, options) {
	var previous = this.store.findRecord('is_active', true);
	console.log('previous: ', previous.get('title'), selections);
	previous.set('is_active', false);
	this.record = selections[0];
	this.record.set('is_active', true);
	console.log('current: ', this.record.get('title'));
    }
});