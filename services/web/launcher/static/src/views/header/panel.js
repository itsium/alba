Ext.define('App.views.header.Panel', {
    extend: 'Ext.container.Container',
    alias: 'widget.app.views.header.panel',

    initComponent: function() {
	this.layout = {
	    type: 'hbox',
	    pack: 'start',
	    align: 'center'
	};
	this.height = 40;
	this.cls = 'header-container';
	this.items = [{
	    xtype: 'container',
	    width: 150,
	    html: '<div class="header-title">alba</div>'
	}, {
	    xtype: 'container',
	    flex: 1
	}, {
	    xtype: 'container',
	    width: 20,
	    margins: '10 3 3 3',
	    html: '<img width="20px" src="/launcher/static/resources/images/106-sliders.png" />'
	}];
	this.callParent(arguments);
    }
});