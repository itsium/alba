Ext.define('App.views.process.Listing', {
    alias: 'widget.app.views.process.listing',
    extend: 'Ext.panel.Panel',

    initComponent: function() {
	this.layout = 'card';
	this.activeItem = 0;
	this.title = 'Processes';
	this.iconCls = 'applications-stack';
	this.dockedItems = [{
	    xtype: 'toolbar',
	    dock: 'top',
	    border: false,
	    items: [{
		xtype: 'button',
		iconCls: 'application-plus',
		enableToggle: true,
		toggleGroup: 'process-treelist-toggle',
		scope: this,
		toggleHandler: this.availableToggle,
		allowDepress: false,
		pressed: true,
		text: 'Available'
	    }, '-', {
		xtype: 'button',
		enableToggle: true,
		toggleGroup: 'process-treelist-toggle',
		scope: this,
		allowDepress: false,
		toggleHandler: this.runningToggle,
		iconCls: 'application-arrow',
		text: 'Running'
	    }]
	}];
	this.items = [{
	    xtype: 'app.views.process.available',
	    border: false,
	    header: false,
	    layout: 'fit'
	}, {
	    xtype: 'app.views.process.running',
	    border: false,
	    header: false,
	    layout: 'fit'
	}];
	this.callParent.apply(this, arguments);
    },

    availableToggle: function(button, pressed) {
	if (pressed === true) {
	    this.layout.setActiveItem(0);
	}
    },

    runningToggle: function(button, pressed) {
	if (pressed === true) {
	    this.layout.setActiveItem(1);
	}
    }
});