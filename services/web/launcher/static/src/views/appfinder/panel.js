Ext.define('App.views.appfinder.Panel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.app.views.appfinder.panel',

    initComponent: function() {
	this.header = false;
	this.border = false;
	this.bodyStyle = 'background: transparent; margin: 10px;',
	this.layout = 'border';
	this.store = Ext.create('App.models.ApplicationStore');
	this.items = [{
	    xtype: 'app.views.appfinder.search',
	    region: 'north',
	    height: 35,
	    store: this.store
	}, {
	    xtype: 'app.views.appfinder.results.detail',
	    region: 'center',
	    store: this.store
	}, {
	    xtype: 'app.views.appfinder.results.list',
	    width: 210,
	    region: 'west',
	    store: this.store
	}];
	this.callParent(arguments);
    }
});