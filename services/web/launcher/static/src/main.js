Ext.onReady(function() {
    Ext.QuickTips.init();

    Ext.application({
	name: 'Alba',
	launch: function() {
            Ext.create('Ext.container.Viewport', {
		layout: 'border',
		items: [{
		    xtype: 'app.views.header.panel',
		    region: 'north',
		    margins: '0 0 0 0'
		}, {
		    xtype: 'app.views.appfinder.panel',
		    region: 'center',
		    margins: '0 0 0 0'
		}, {
		    xtype: 'app.views.launcher.panel',
		    region: 'west',
		    margins: '0 0 0 0'
		}]
            });
	}
    });
});
