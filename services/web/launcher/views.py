"""
Alba index blueprint.
"""
import json
import alba.api.app

from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound


__launcher__ = Blueprint('launcher', __name__,
                         static_folder='static',
                         static_url_path='/static',
                         template_folder='templates',
                         url_prefix='/launcher')


@__launcher__.route('/')
def show():
    """
    Show given page if exists.
    """
    try:
        return render_template('launcher/index.html')
    except TemplateNotFound:
        abort(404)


@__launcher__.route('/app/list')
def app_list():
    """
    Return all available applications.
    """
    apps = alba.api.app.listall()
    result = {'results': apps}
    return json.dumps(result)
