"""
alba logging config.
"""
import os
import sys
sys.path.insert(0, os.path.abspath('%s/../..' % os.path.dirname(os.path.abspath(__file__))))

import alba.api.process


class Service(alba.api.process.App):
    """Configuration for logging service."""
    name = 'logging'
    category = 'services'
    description = 'Logging service'
    synopsis = 'Logging service'
    socket = 'tcp://127.0.0.1:50001'


app = Service(__name__, __file__)


@app.add_argument('message', nargs='+', help='''
Message to log.
''')
@app.route('log')
def log(api, args):
    message = str(args.get('message', ''))
    if api.config_get('core_debug', False) is True:
        print message


if __name__ == '__main__':
    if len(sys.argv) > 1:
        app.boot(sys.argv[1])
