	       ___    __
	      /\_ \  /\ \
	   __ \//\ \ \ \ \____    __
	 /'__`\ \ \ \ \ \ '__`\ /'__`\
	/\ \L\.\_\_\ \_\ \ \L\ \\ \L\.\_
	\ \__/.\_\\____\\ \_,__/ \__/.\_\
	 \/__/\/_//____/ \/___/ \/__/\/_/

	------ Where is Jessica !? ------


REQUIREMENTS
------------

	pip install setproctitle
	pip install psutil
	#pip install pymongo
	pip install pyparsing
	pip install pyzmq (sudo apt-get install libzmq)


TODO V1
-------

1. Interface web par defaut pour toutes les apps.
 + Generer une UI extjs basique pour les apps n'en fournissant pas.

2. Shell extjs identique au shell alba.shell.
 + Reprendre les methods du shell alba.shell et le faire en extjs.
   Cela est une app en soit, il faudra juste share/refactoriser les methodes
   de l'alba.shell (qui pourra utiliser l'app en question a l'avenir).

3. Chiffrer les transactions entre zeromq + web avec AES
 + Mettre dans la configuration de l'application sa cle AES 256
   et ensuite l'utiliser pour chiffrer les communications zeromq.
   Pour l'interface web, la cle sera demande au chargement de la page
   et ensuite utiliser pour l'envoie de donnee au serveur et reception des
   donnees.

4. Refactoriser le code.
 + Histoire que ca brille.

5. Gestion des exceptions.
 + Pour l'instant les exceptions generees sont dans un fichier de logs.
   On ne les vois par dans l'UI et en plus certaines exception ne sont pas
   push dans les fichiers de logs.
   Voir comment catcher proprement les exceptions et en informer l'utilisateur.

6. Tests unitaires.
 + Ecrire des tests unitaires permettant de tester la stabilitee du core ainsi
   que celle des apps.

7. Documentation.
 + Documenter le code ainsi qu'ajouter des exemples d'apps permettant
   d'avoir un panel assez large d'utilisation de l'API alba.

8. Generer les formulaires extjs en fonction des @add_argument.
 + Le shell dispose deja des options en ligne de commande,
   mais pour l'interface (par defaut ou non) d'extjs,
   il faudra alors generer un formulaire (pour chaque route dispo)
   en fonction des @add_argument qui sont specifie.

9. Shutdown propre
 + Lors d'un `KeyboardException` ou d'un appel a `quit` fermez proprement
   tous les processes ainsi que le server RPC et join tous les threads/processes.
