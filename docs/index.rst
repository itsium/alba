.. alba documentation master file, created by
   sphinx-quickstart on Thu Apr 26 16:26:03 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to alba's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2
.. automodule:: alba.app
   :members:
.. automodule:: alba.rpc.api.baseserver
   :members:
.. automodule:: alba.rpc.api.baseclient
   :members:
.. automodule:: alba.api.core
   :members:
.. automodule:: alba.settings.config
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

